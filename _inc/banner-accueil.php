<div id="bandeau-accueil" class="accueil">

<div id="accueil" class="actualites">
  <h1><a href="?page_id=2697">Actualités</a></h1>
  <a href="?page_id=2697" id="pixees-actu"></a>
  <p>On reste informé <br/>grâce à la gazette <br/>des sciences du numérique <br/>et à ses archives</p>
</div>

<div id="accueil" class="ressources">
  <h1><a href="?page_id=36">Ressources</a></h1>
  <a href="?page_id=36" id="pixees-ressources"></a>
  <p>On utilise des ressources <br/>clés en main <br/>qui ont fait leurs preuves <br/>sur le terrain.</p>
</div>

<div id="accueil" class="formations">
  <h1><a href="?page_id=9822">Formations</a></h1>
  <a href="?page_id=9822" id="pixees-formations"></a>
   <p>On se forme <br/>pour initier les jeunes <br/>à la pensée informatique.</p>
</div>

<div id="accueil" class="interventions">
  <h1><a href="?page_id=40">Interventions</a></h1>
  <a href="?page_id=40" id="pixees-interventions"></a>
  <p>On échange <br/>avec des scientifiques<br/>tchat, conférences, <br/>formations, ateliers</p>
</div>

</div>
