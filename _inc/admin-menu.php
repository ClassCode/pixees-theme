<?php 

//
// Pixees admin menu functionnalities
//

// Manage les categories des gazettes

function pixees_admin_menu_manage_news() 
{
  $log = "<ul>";
  // - retire de la une les actualités du mois précédent,
  {
    foreach (pixees_admin_menu_get_category_posts(get_category_by_slug('actualites-a-la-une')) as $post) {
      pixees_admin_menu_set_category($post->ID, 'actualites-a-la-une', false);
    }
  }
  // - catégorie de gazette la plus récente
  $cat_last = false;
  // - balaye toutes les catégories pour 
  // -- déclarer obsolète les actualités de plus d´un an,
  // -- identifier la categorie la plus recente,
  {
    // Calcule la date actuelle en mois : annee x 12 + mois 
    $today = getdate(); $now = $today['mon'] + $today['year'] * 12;
    // Détecte les gazettes comme des categories commençant par une année avec une virgule 20xx,
    foreach(pixees_admin_menu_get_categories() as $cat) {
      if (preg_match("/20..,.*/", $cat->name)) {
	// Calcule la date de la gazette en mois : annee x 12 + mois à parir de la syntaxe «2015, Avril» qui est à respecter
	$when = 
	  array_search(preg_replace("/[^A-Za-z]/", "",  str_replace("é", "e", str_replace("û", "u", $cat->name))),
		       array('Zero', 'Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre')) +
	  preg_replace("/(20..).*/", "$1",  $cat->name) * 12;
	// Gère les gazettes obsolètes
	{
	  if ($when < $now - 12)
	    foreach(pixees_admin_menu_get_category_posts($cat) as $post) {
	      $actu = true;
	      foreach(array('thematique', 'support-pedagogique', 'public-cible','interventions') as $cat_slug)
		$actu = $actu && !pixees_admin_menu_has_post_category($post, $cat_slug);
	      if ($actu) 
		custom_post_status_set($post->ID, 'obsolete');
	    }
	}
	// Détermine la gazette la plus récente
	{
	  if($when <= $now && ((!$cat_last) || $cat_last->name < $cat->name))
	    $cat_last = $cat;
	}
      }
    }
  }
  // - publie et mets à la une toutes les actualités du mois,
  if ($cat_last) {
    foreach (pixees_admin_menu_get_category_posts($cat_last) as $post) {
      pixees_admin_menu_set_category($post->ID, 'actualites-a-la-une', true);
      custom_post_status_set($post->ID, 'publish');
    }
    $log .= "<li>-Catégories des actualités mises à jour, <i><a href='edit.php?category_name=".$cat_last->slug."'>".$cat_last->name."</a></i> est à la une</li>";
  } else {
    $log .= "<li>Upps, on a pas pu déterminer la catégorie des actualités à la une.</li>";
  }
  // Id de la page actualité
  return $log."</ul>";
}

//
// Admin menu utilitary routines
//

// Returns all categories in decreasing order
function pixees_admin_menu_get_categories() 
{
  return get_categories(array(
			      'type'               => 'post',
			      'child_of'           => 0,
			      'orderby'            => 'name',
			      'order'              => 'DESC',
			      'parent'             => '',
			      'hide_empty'         => 0,
			      'hierarchical'       => 1,
			      'taxonomy'           => 'category',
			      'pad_counts'         => false 
			      ));
}
// Returns all posts of a category as an array
function pixees_admin_menu_get_category_posts($cat)
{
  $posts = get_posts(array(
			   'posts_per_page'   => 10000,
			   'offset'           => 0,
			   'category'         => $cat->term_id,
			   'post_type'        => 'post',
			   'post_status'      => 'any',
			   'suppress_filters' => false 
			   ));
  return $posts ? $posts : array();
}
function pixees_admin_menu_get_obsolete_posts($republish = false)
{
  $posts = get_posts(array(
			   'posts_per_page'   => 10000,
			   'offset'           => 0,
			   'category'         => 0,
			   'post_type'        => 'post',
			   'post_status'      => 'obsolete',
			   'suppress_filters' => false 
			   ));
   if ($republish)
    foreach($posts as $post)
      custom_post_status_set($post->ID, 'publish');
  return array_map(function($post) { return $post->post_title; }, $posts ? $posts : array());
}
// Returns true if a post belongs to a given category
function pixees_admin_menu_has_post_category($post, $cat_slug)
{
  foreach(get_the_category($post->ID) as $cat) 
    foreach(explode("/", get_category_parents($cat->term_id, false, "/", true)) as $slug)
      if ($slug == $cat_slug)
	return true;
  return false;
}
// Sets or unsets a category of a given post
function pixees_admin_menu_set_category($post_id, $cat_slug, $add_else_delete)
{
  $cat_id = get_category_by_slug($cat_slug)->term_id;
  $post_cat_ids = wp_get_post_categories($post_id);
  $key = array_search($cat_id, $post_cat_ids);
  if ($key) {
    if (!$add_else_delete) {
      unset($post_cat_ids[$key]);
      wp_set_post_categories($post_id, $post_cat_ids);
    }
  } else {
    if ($add_else_delete) {
      $post_cat_ids[] = $cat_id;
      wp_set_post_categories($post_id, $post_cat_ids);
    }
  }
}
/* Applies a regex transform on a post content
function pixees_admin_menu_preg_replace($pattern, $replacement, $post_id, $limit = 1)
{
  $post = get_post($post_id);
  $post->post_content = preg_replace($pattern, $replacement, $post->post_content, $limit);
  wp_update_post($post);
}
*/

//
// Admin menu implementation
//

// Admin menu http interface
function pixees_admin_menu_http_request() 
{
  $notices = array();
  if (isset($_REQUEST['pixees_admin_menu_manage_news'])) {
    $notices[] = array('updated' => pixees_admin_menu_manage_news());
  }
  if (isset($_REQUEST['pixees_admin_root_mode'])) {
    update_option('pixees-theme/root-mode', $_REQUEST['pixees_admin_root_mode'] == "true");
  }
  /* juste utilise pour depannage
  if (isset($_REQUEST['pixees_admin_menu_obsolsecence_kill_news'])) {
    $notices[] = array('updated' => 'Modified posts, cancelling obsolsecence: '.implode("|", pixees_admin_menu_get_obsolete_posts(true)));
  }
  */
  return $notices;
}
// Admin menu notice
function pixees_admin_menu_notice()
{
  $notices = pixees_admin_menu_http_request();
  // Displays all notices of the form array(array($class => $message), .) with $class = {'updated', 'error'}
  foreach ($notices as $notice)
    foreach($notice as $class => $message)
      echo "<div class='$class'><p>$message.</p></div>";
}
// Admin menu html
function pixees_admin_menu_html()
{
  $site = get_site_url();

  echo "<h1>Panneau de lancement des outils algorithmes de Pixees</h1>";
  // 
  echo "<hr style='clear:both;'/><h2>Mise à jour des actualités</h2><a style='float:right;margin: 20px 60px 0px 0px' class='button button-primary' href='tools.php?page=pixees_admin&pixees_admin_menu_manage_news=true'>Mettre à jour les actualités</a><div style='margin: 0px 40px 40px 40px'>- retire de la une les actualités du mois précédent,<br/>- publie et mets à la une toutes les actualités du mois,<br/>- déclare obsolète les actualités de plus d´un an,<br/>- met à jour le lien vers la dernière gazette sur la page d´actualité.</div>";
  // 
  // juste utilisé pour dépannage echo "<hr style='clear:both;'/><h2>Annuler l'obsolescence de tous les contenus.</h2><h3 align='center'>ATTENTION C EST DEFINITIF!</h3><a style='float:right;margin: 20px 60px 0px 0px' class='button button-primary' href='tools.php?page=pixees_admin&pixees_admin_menu_obsolsecence_kill_news=true'>Anuller toutes les obsolescence</a><br style='clear:both;'/>";
  //
  echo "<hr style='clear:both;'/><h2>Gestion des unes de pixees</h2><table style='margin: 0px 40px 40px 40px'>
   <tr><th align='left'>Accueil</th><td><a class='button' href='$site?page_id=2775'>Voir la page</a></td><td>(voir la <a href='$site?cat=628'>catégorie</a>)</td><td><a class='button' href='edit.php?category_name=a-decouvrir'>Éditer les items</a></td></tr>
   <tr><th align='left'>Actualité</th><td><a class='button' href='$site?page_id=2697'>Voir la page</a></td><td>(voir la <a href='$site?cat=604'>catégorie</a>)</td><td><a class='button' href='edit.php?category_name=actualites-a-la-une'>Éditer les items</a></td></tr>
   <tr><th align='left'>Ressources</th><td><a class='button' href='$site?page_id=36'>Voir la page</a></td><td>(voir la <a href='$site?cat=609'>catégorie</a>)</td><td><a class='button'  href='edit.php?category_name=ressources-a-la-une'>Éditer les items</a></td></tr>
   <tr><th align='left'>Actualité</th><td><a class='button' href='$site?page_id=40'>Voir la page</a></td><td>(voir la <a href='$site?cat=610'>catégorie</a>)</td><td><a class='button' href='edit.php?category_name=interventions-a-la-une'>Éditer les items</a></td></tr>
   </table>";
  //  
  echo "<hr style='clear:both;'/><h2>Gestion de la visibilité des réglages du site</h2>";
  $value = get_option('pixees-theme/root-mode', false);
  echo "<a style='margin-left:20px;' class='button' href='tools.php?page=pixees_admin&pixees_admin_root_mode=".($value ? "false" : "true")."'>".($value ? 'Fermer' : 'Ouvrir')." les réglages du site</a>";
  //
  echo "<hr/>";
}
// Admin menu hooks wrapping 
function pixees_admin_menu() 
{
  add_management_page('Pixees', 'Pixees', 'manage_options', 'pixees_admin', 'pixees_admin_menu_html');
}
add_action('admin_menu', 'pixees_admin_menu', '20.8988');
add_action('admin_notices', 'pixees_admin_menu_notice');
?>
