<div id="loginFooter">
<?php 
   $once_gain = isset($_REQUEST['action']) && in_array($_REQUEST['action'], array('register','lostpassword','rp','resetpass'));
  if($once_gain){
?>
Pour pouvoir b&eacute;n&eacute;ficier de toutes les fonctionalit&eacute;s de Class'Code, notamment en ce qui concerne les formations, il est n&eacute;cessaire de s'inscrire. Le traitement et stockage des données a été déclaré au Correspondant Informatique et Liberté Inria.
<?php
  }else{
?>
<div class='subtitle'><b>Pour <a style="color:#5D5D5D"  href="https://pixees.fr/wp-login.php?action=register">s'inscrire c'est <u>par ici</u></a>.</b></div>
Le saviez vous ? En cochant "Se souvenir de moi" vous n'aurez pas à rentrer votre mot de passe à chaque visite. Les informations permettant de vous identifier seront stockées sur votre ordinateur, pas de risque que l'on utilise votre mot de passe ! Les informations personnelles font l'objet d'un dépôt à la CNIL.
<?php  
  }
?>
<script>
 
  document.getElementById("backtoblog").innerHTML = "";

  if (document.getElementById("loginform") != undefined)
    document.getElementById("loginform").innerHTML = document.getElementById("loginform").innerHTML.replace(/Nom d’utilisateur ou adresse e-mail/, 'Identifiant ou e-mail');

  if(document.getElementById("login").getElementsByClassName('message')[0] != undefined)
    document.getElementById("login").getElementsByClassName('message')[0].innerHTML = "Vous venez de demander un mot de passe pour Class´Code.<ul style='padding-left:20px'><li>Vous allez recevoir un email qui va vous permettre de le choisir en cliquant (une fois) sur le lien proposé.</li><li>Note: <i>bien cliquer sur ``réinitialiser le mot de passe´´</i>.</li><li>Vous pourrez alors vous connecter avec votre identifiant et ce mot de passe.</li></ul>En cas de problème, contacter <a href='https://classcode.fr/accueil/aide?cc_mailer_shortcode_to=classcode-accueil%40inria.fr&cc_mailer_shortcode_who=Bureau%20d%C2%B4Accueil%20de%20Class%C2%B4Code&cc_mailer_shortcode_subject=J%C2%B4ai%20un%20souci%20pour%20me%20connecter%20%C3%A0%20la%20plateforme%20https%3A%2F%2Fclasscode.fr&cc_mailer_shortcode_body=&body_completion=true#aideAction2'>classcode-accueil@inria.fr</a>, nous sommes à votre disposition.<br/>À bientôt sur Class´Code!";
<?php
  if($_REQUEST['action'] == 'rp'){
?>  
    jQuery("#header").after("<p class='message'> Voici votre mot de passe, <br/>- vous pouvez le changer si vous souhaitez un mot de passe plus lisible, <br/>- surtout: copiez le bien en lieu sûr dans tous les cas, avant de taper ``Réinitialiser ..´´<br/>Vous pouvez alors vous connecter.</p>");
<?php  
  }else if ($_REQUEST['action'] == 'lostpassword'){
?>
    jQuery( "#user_login" ).remove();
    jQuery("label[for=user_login]").html('<input name="user_login" id="user_login" class="input" value="" size="20" type="text" placeholder="Identifiant ou e-mail">');
    var formheader ="";
    formheader+="<h2>Mot de passe perdu</h2>";
    formheader += "<div id='loginformheader'>Récupérer son mot de passe<span class='loginheadersymbol'>></span></div>";
    jQuery(formheader).insertBefore("#lostpasswordform");
<?php  
  }else if ($_REQUEST['action'] == 'register'){
?>
    jQuery( "#user_login" ).remove();
	  jQuery( "#user_pass" ).remove();
    jQuery("#user_email").remove();
    jQuery("label[for=user_login]").html('<input name="user_login" id="user_login" class="input" value="" size="20" type="text" placeholder="Identifiant">');
    jQuery("label[for=user_email]").html('<input name="user_email" id="user_email" class="input" value="" size="20" type="text" placeholder="Adresse de messagerie">');
    var formheader ="";
    formheader+="<h2>S'inscrire</h2>";
    //    formheader += "<div class='subtitle'>Inscrivez-vous sur Class'Code et bénéficier d'un accès illimité à OpenClassrooms pendant 1 mois !</div>";
    formheader += "<div id='loginformheader'>Créer un compte Class'Code<span class='loginheadersymbol'>></span></div>";
    jQuery(formheader).insertBefore("#registerform"); 

<?php
}else{
?>
  if (document.getElementById("loginform") != undefined){
    jQuery( "#user_login" ).remove();
	  jQuery( "#user_pass" ).remove();
    jQuery("label[for=user_login]").html('<input name="log" id="user_login" class="input" value="" size="20" type="text" placeholder="Identifiant ou e-mail">');
    jQuery("label[for=user_pass]").html('<input name="pwd" id="user_pass" class="input" value="" size="20" type="password" placeholder="Mot de passe"><div id="passwd_forgotten"><a href="<?php echo wp_lostpassword_url(get_permalink()); ?>">mot de passe oublié ?</a></div>');
    jQuery("#wp-submit").val("Valider");
    var formheader ="";
    
    formheader+="<h2>Se connecter</h2>";
    formheader += "<div class='subtitle'>Première visite ? Cliquez <a href=\"<?php echo get_site_url();?>/wp-login.php?action=register\">ici pour s´incrire</a> !</div>";
    formheader += "<div class='subtitle'>Déjà inscrit sur OpenClassrooms, connectez-vous directement !</div>";
    formheader += "<a title=\"Attention si je je suis déjà inscrit aussi sur cette plateforme, cela me connectera avec un autre compte\" href=\"<?php echo get_site_url();?>/wp-content/plugins/class_code/oc_api\"><div id='loginOOCheader'>Avec un compte OpenClassrooms<span class='loginheadersymbol'>></span></div></a><br>";
    formheader += "<div id='loginformheader'>Avec un compte Class'Code<span class='loginheadersymbol'>></span></div>";
    
    jQuery( formheader ).insertBefore("#loginform"); 
  }
<?php
}
?>  

</script>


</div>