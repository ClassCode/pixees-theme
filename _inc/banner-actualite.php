<div id="bandeau" class="actualite">
  <div id="bandeau-img"><a href="?page_id=2697" id="pixees-actu"></a></div>
  <div id="bandeau-txt"><h1>Actualités</h1><p>On reste informé grâce à la gazette des sciences du numérique</p></div>
<?php  
   // Affiche le lien vers la dernière gazette
  {
    // Identifie le bon id
    $email_id = 84; // Ici on le met à la main au cas où mais ci dessous il essaye de le calculer automatiquement
    { 
      global $wpdb;
      $email_ids = $wpdb->get_results('SELECT email_id, subject FROM wp_wysija_email WHERE sent_at IS NOT NULL ORDER BY email_id DESC');
      foreach($email_ids as $email_id_i) 
        if (preg_match('/Médiation scientifique en Sciences du Numérique : la gazette/', $email_id_i->subject)) {
          $email_id = $email_id_i->email_id;
          break;
      }
    }
    $link = get_site_url()."/?wysija-page=1&controller=email&action=view&email_id=$email_id&wysijap=subscriptions";       
    echo '<div id="bandeau-more-actu"><a target="_blank" href="'.$link.'"><img style="width:250px;padding:0px;margin:0px" src="'.get_site_url().'/wp-content/themes/pixees-theme/_img/lien-ext-gazette.png" alt="La gazette du mois"></a></div>';  
  }
?>
</div>