<div style="margin:100px;">
  <h1>Rien de trouvé !
   <p>Il semblerait que rien n´ai été trouvé à cette adresse, 
   <ul><li>essayez peut-être une recherche ou</li></ul>
   <ul><li>revenez à la page d´accueil.</li></ul>
  </p></h1>

<p>Vous arrivez sur cette page non pas à cause d´une erreur interne au site pixees.fr mais parce que le lien que vous venez de cliquer est erronné. Revenez à la page précédente et, s´il vous plaît, signalez le souci à l´auteur de la page qui contient ce lien erroné.</p>
</div>