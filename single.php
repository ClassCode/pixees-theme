<?php 

$is_classcode_v2_is_online = $GLOBALS['classcode_v2_is_online'];

$post_a_la_carte = is_post_a_la_carte( $post ) && $is_classcode_v2_is_online;
update_post_meta( $post->ID, "post_a_la_carte", $post_a_la_carte );

$isPageClassCode2 = $_REQUEST['context'] == 'classcode2';

if (get_post()->post_type == 'rencontre') {
  $post_rencontre = true;
}else{
  $post_rencontre = false;
}

?>

<?php get_header(); ?>

<?php if ( !$isPageClassCode2 && !$post_a_la_carte) : ?>
<div id="conteneur"><section id="contenu">
<?php endif ?>

<?php 

    // @vthierry/rencontre: hook to display a rencontre
    if ($post_rencontre) {
      $_REQUEST['post_id'] = get_post()->ID;
      if( $is_classcode_v2_is_online ){
        include_once(get_template_directory().'/../../plugins/class_code_v2/templates/template-creer-rencontre.php');
      }else{
        include_once(get_template_directory().'/../../plugins/class_code/rencontre/rencontre-edit.php');
      }
    } else if ($post_a_la_carte) {
        include_once(get_template_directory().'/../../plugins/class_code_v2/templates/template-post.php');
    } else 
      the_theme_posts(true, true); 
?>
    
<?php if ( !$isPageClassCode2 && !$post_a_la_carte && !$post_rencontre) : ?>
</section><?php get_sidebar(); ?></div>
<?php endif ?>
    
<?php get_footer(); ?>
