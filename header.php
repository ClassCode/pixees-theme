<?php header('X-Frame-Options: GOFORIT'); ?>
<?php include_once(get_template_directory().'/_inc/display-functions.php'); ?>
<?php
  // Defines the context: "presentation", "accueil", "actualite", "ressource", "intervention", "contact".
  if (!isset($_REQUEST['context'])) $_REQUEST['context'] = get_theme_context();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>    
    <meta name="viewport" content= "width=device-width, initial-scale=1.0">
    <title><?php if (!is_404()) the_title(); ?> — <?php echo $_REQUEST['context'] == 'classcode' ? 'Class´Code' : 'Pixees'; ?></title>
    <link href="<?php the_theme_file('/style.css');?>" type="text/css" rel="stylesheet" />
<?php    
  if ($_REQUEST['context'] == 'classcode') { 
?>
    <link href="<?php the_theme_file('/classcode.css');?>" type="text/css" rel="stylesheet" />
  
<?php 
if (get_post_depth() > 2) {
?>
<style> .classcode { padding-top:10px; padding-left:16%;   padding-right:16%; width:auto; }</style>
<?php  
}
  }
?>
    <link href='https://fonts.googleapis.com/css?family=Francois+One' rel='stylesheet' type='text/css'/>

    <?php wp_head(); ?>

<!-- Loading shadowbox -->
<link rel="stylesheet" type="text/css" href="<?php the_theme_file('/shadowbox/shadowbox.css');?>">
<!--script type="text/javascript" src=".../jquery-3.0.0.min.js"></script-->
<script type="text/javascript" src="<?php the_theme_file('/shadowbox/shadowbox.js');?>"></script>
<script type="text/javascript">Shadowbox.init();</script>

  </head>
  <body id="body" onresize="onPageResize()">

 <script language='javascript'>
 //dropdown menu for  login
 function loginDropdownOpen() {
   if (document.getElementById("loginDropdown") != null)
    document.getElementById("loginDropdown").classList.toggle("show");
 }
 function formationDropdownOpen() {
   if (document.getElementById("formationDropdown") != null)
   document.getElementById("formationDropdown").classList.toggle("show");
 }
 

  // Close the dropdown menu if the user clicks outside of it
  window.onclick = function(event) {
    try {
      if (!event.target.matches('.dropMenuLink')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) { openDropdown.classList.remove('show'); }
        }
      }
    }catch(err){
      
    }
  }


 // Manages the display of open/close elements
 function onPageResize() {
   <?php if ($_REQUEST['context'] != 'classcode' && $_REQUEST['context'] != 'classcode2'){
?>
   if (document.getElementById("menu") != null)
     document.getElementById("menu").style.display = window.innerWidth < 1150 ? 'none' : 'initial';
 <?php }
?>
  }
 function elementToogle(id, disp) {
   if (document.getElementById(id) != null)
   document.getElementById(id).style.display = document.getElementById(id).style.display == 'none' || document.getElementById(id).style.display == '' ? disp : 'none';
 }
 // Magages cookies, from http://www.w3schools.com/js/js_cookies.asp
 function getCookie(name) {
   var cookies = document.cookie.split(';');
   for(var i = 0; i < cookies.length; i++) {
     var cookie = cookies[i].trim();
     if (cookie.indexOf(name + "=") == 0) return cookie.substring(name.length + 1);
   }
   return "";
 } 
function setCookie(name, value) {
  document.cookie = name + "=" + value;
}
</script>

<?php 
	  
  if ( ! is_classcode2( $post) ) { ?>

<header id="header">
   
  <?php 
    if ($_REQUEST['context'] == 'classcode') {
      $user_logged_in = is_user_logged_in();
     // include(get_template_directory().'/_inc/classcode-banner.php'); 
      if (!isset($_REQUEST['httpsyndicationwrap'])) {
	include(get_template_directory().'/_inc/classcode-barreliens.php'); 
	//- include(get_template_directory().'/_inc/classcode-menu.php'); 	
      }
    } else {
      include(get_template_directory().'/_inc/site-banner.php'); 
      include(get_template_directory().'/_inc/site-menu.php'); 
    }
  ?>  

  

  <?php 
    $banner = get_template_directory().'/_inc/banner-'.$_REQUEST['context'].'.php';
    if (is_file($banner)) {
      include($banner); 
    } 
  ?>
</header>  

<?php } ?>
