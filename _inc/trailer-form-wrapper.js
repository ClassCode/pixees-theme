
// This functions set the value of an element using the http query parameter
var decodeHTMLEntities = function(encodedString) {
  var textArea = document.createElement('textarea');
  textArea.innerHTML = encodedString;
  return textArea.value;
}
var getQueryVar = function(name) {
  if (document.getElementById(name) != null)
    if (document.URL.match("[?&]"+name+"=")) {
      var value = decodeHTMLEntities(decodeURIComponent(document.URL.replace(new RegExp(".*[?&]"+name+"=([^&#]*).*"),"$1")));
      if (document.getElementById(name).tagName == "TEXTAREA")
	document.getElementById(name).innerHTML = value;
      else
	document.getElementById(name).value = value;
    }
}
