<?php include(get_template_directory().'/_inc/trailer-link.php');  ?>

<div id="trailer">
  <div id="participez-ht"><span id="pixees-participez-haut"></span></div>
  <section id="participez">  
    <span id="pixees-participez-bas"></span>
    <h1> participez !</h1>
    <ul>
      <li><a target="_blank" href="<?php echo $http_question;?>" id="question">une question ?</a></li>
      <li><a target="_blank" href="<?php echo $http_comment;?>" id="commentaire">un commentaire ?</a></li>
      <li><a target="_blank" href="<?php echo $http_retour;?>" id="note">cela vous a été utile ?</a></li>
    </ul>
  </section>
</div>
