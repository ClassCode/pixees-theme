<?php include(get_template_directory().'/_inc/trailer-link.php'); ?>

<aside id="sidebar">

  <section> 	  
    <h1>Interventions</h1>
    <ul>
      <li><h4><?php the_theme_context_link("page_id=40", "À la une"); ?></h4></li>
      <li><h4><?php the_theme_context_link("cat=483", "Conférences"); ?></h4></li>
      <li><h4><?php the_theme_context_link("cat=491", "Ateliers"); ?></h4></li>
      <li><h4><?php the_theme_context_link("cat=605", "Formations"); ?></h4></li>
      <li><h4><?php the_theme_context_link("cat=485", "Débats échanges"); ?></h4></li>
    </ul>
  </section>

  <section> 	  
   <h1>En savoir plus</h1>
      <li><h4><a target="_blank" href="<?php echo $http_intervention;?>">Demander une intervention</a></h4></li>
      <!--li><h4><?php the_theme_context_link("page_id=2332", "Carte de France des acteurs"); ?></h4></li-->
      <li><h4><?php the_theme_context_link("page_id=2312", "La médiation scientifique Inria"); ?></h4></li>
      <li><h4><?php the_theme_context_link("page_id=2329", "Des exemples d´interventions"); ?></h4></li>
  </section>

  <div id="line-sidebar"></div>

  <?php include(get_template_directory().'/_inc/sidebar-item-header.php'); ?>
  
</aside>
