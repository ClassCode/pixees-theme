#!/bin/bash
if [ "$HOSTNAME" == 'qlf-medsciu18' ] ; then
  if [ "$1" == 'qualif' ] 
  then
    if [ "$2" == 'pixees-theme' ]
    then
      echo "Deployment of pixees-theme on qualif"
      cd /appli/www/pixees/wp-content/themes/pixees-theme
      git pull
    elif [ "$2" == 'classcodev1' ]      
    then
      echo "Deployment of class_code plugin on qualif"
      cd /appli/www/pixees/wp-content/plugins/class_code
      git pull
    elif [ "$2" == 'classcodev2' ]
    then
      echo "Deployment of class_code_v2 plugin on qualif"
      cd /appli/www/pixees/wp-content/plugins/class_code_v2
      git pull
    elif [ "$2" == 'all' ]    
    then
      echo "Deployment of pixees-theme on qualif"
      cd /appli/www/pixees/wp-content/themes/pixees-theme
      git pull
      echo "Deployment of class_code plugin on qualif"
      cd /appli/www/pixees/wp-content/plugins/class_code
      git pull
      echo "Deployment of class_code_v2 plugin on qualif"
      cd /appli/www/pixees/wp-content/plugins/class_code_v2
      git pull
    fi
  elif [ "$1" == 'prod' ] 
  then
    echo "Nothing to do on qualif, deployment on prod asked"
  fi
fi
if [ "$HOSTNAME" == 'medsciu18' ] ; then
  if [ "$1" == 'prod' ] 
  then
    if [ "$2" == 'pixees-theme' ]
    then
      echo "Deployment of pixees-theme on prod"
      cd /appli/www/pixees/wp-content/themes/pixees-theme
      git pull
    elif [ "$2" == 'classcodev1' ]
    then
      echo "Deployment of class_code plugin on prod"
      cd /appli/www/pixees/wp-content/plugins/class_code
      git pull
    elif [ "$2" == 'classcodev2' ]
    then
      echo "Deployment of class_code_v2 plugin on prod"
      cd /appli/www/pixees/wp-content/plugins/class_code_v2
      git pull
    elif [ "$2" == 'all' ]    
    then
      echo "Deployment of pixees-theme on prod"
      cd /appli/www/pixees/wp-content/themes/pixees-theme
      git pull
      echo "Deployment of class_code plugin on prod"
      cd /appli/www/pixees/wp-content/plugins/class_code
      git pull
      echo "Deployment of class_code_v2 plugin on prod"
      cd /appli/www/pixees/wp-content/plugins/class_code_v2
      git pull
    fi
  elif [ "$1" == 'qualif' ] 
  then
    echo "Nothing to do on prod, deployment on qualif asked"
  fi
fi
