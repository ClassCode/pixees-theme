<aside id="sidebar">

  <section> 	 
    <h1>Contacts</h1> 
    <ul>
      <li><h3><?php the_theme_context_link("page_id=42", "Nous contacter"); ?></h3></li>
      <li><h4><?php the_theme_context_link("page_id=2749", "Formulaire de contact"); ?></h4></li>
      <li><h4><?php the_theme_context_link("page_id=2781", "Pour les TPE/TIPE"); ?></h4></li>
      <li><h4><?php the_theme_context_link("page_id=2312", "La médiation scientifique Inria"); ?></h4></li>
	  <ul class="medsciMenu">
        <li><h4><?php the_theme_context_link("page_id=20481", "Présentation"); ?></h4></li>
        <li><h4><?php the_theme_context_link("page_id=20520", "Nos projets"); ?></h4></li>
        <li><h4><?php the_theme_context_link("page_id=20510", "Interventions"); ?></h4></li>
        <li><h4><?php the_theme_context_link("page_id=20659", "Ressources"); ?></h4></li>
        <li><h4><?php the_theme_context_link("page_id=20532", "Contacts"); ?></h4></li>
      </ul>
    </ul>
  </section>
 
  <div id="line-sidebar"></div>
  
  <?php include(get_template_directory().'/_inc/sidebar-item-gazette.php'); ?>
  
  <div id="line-sidebar"></div>

  <?php include(get_template_directory().'/_inc/sidebar-item-capmaths.php'); ?>
  
  <div id="line-sidebar"></div>

   <section> 	 
    <ul>
      <li><h3><?php the_theme_context_link("page_id=1980", "Tous les partenaires"); ?></h3></li>
    </ul>
  </section>
 
  <div id="line-sidebar"></div>

</aside>
