# THE THEME, SPECIFIC TO HTTP://PIXEES.FR #

This theme is a minimal, fixed (non-parametric: no widget in sidebar, no variable menu) wordpress theme dedicated to the http://pixees.fr website. 

It has the following features:

* The site has "contexts" home (accueil), news (actualit�), resources, interventions and contacts respectively.
  * Each context defines a specific banner, sidebar and title color.

* Each post has a title, belong to catogires whose roots define the context, key-words (i.e., tags)
  * The *excerpt* is defined as the 600 first letters, the *thumbnail* as the 1st image in the contents and the *main link* as the 1st link in the text, they thus are not user defined.

* Each post allows retrolinks by default, but not comment: users are invited to feedback and interact via a specific form
  * Certain posts open their comments because they are defined as discussion as on a forum.

* The site has a lexic mechanism allowing to display (here: word definitions) in alphabetic order, letter by letter

* The [popdown title="the title"]the body[/popdown] or [popdown post_id="$postid"]shotcode allows to include contents with a on-off bar display


------------------------------------------------------------------------------------------------------

# PIXEES.FR FILE DESCRIPTION #

* `readme.txt` This documentation file in [http://daringfireball.net/projects/markdown markdown syntax].

* Wordpress file wrappers
  * `style.css` the unique CSS specification file, with a correct wordpress theme header
  * `functions.php` contains the backoffice configuration and wordpress function hook
  * `header.php` , `sidebar.php` , `footer.php` , `comments.php` contain the corresponding page elements templates wrappers
  * `archive.php`, `page.php`, `search.php`, `single.php` contain the corresponding content category renderers
  * `index.php` fallback page, should never be used, this would be a theme bug.
  * `home.php` site home page, only a redirection.
  * `404.php` site fallback for unknown location, a simple message.

* Theme template files
  * `_inc/display-functions.php` contains the PHP contents rendering functions (i.e., the wordpress main loop).
  * `_inc/site-banner.php` , `_inc/site-menu.php` , `_inc/site-footer.php` defines the general site top banner, main menu bar and footer.
  * `_inc/banner-*.php` defines the contextual banner for the home, news, resources, interventions and contacts respectively.
  * `_inc/sidebar-*.php` defines the contextual sidebar menu for the home, news, resources, interventions and contacts respectively.
  * `_inc/sidebar-item-*.php` defines subpart of contextual sidebar menus.
  * `_inc/trailer-page.php` and `_inc/trailer-post.php` define page and post trailer allowing users to feedback via a form mecahnism.
    * `_inc/trailer-link.php` and `_inc/trailer-form-wrapper.js` implement the pre-filled form mechanism.
  * `_inc/lexique-header.php` , `_inc/page-contact.php` , `_inc/site-404.php` are specific rendering tools for the lexic, a predifned page and the unknown location message.
  * `_img/*` all theme icons and images.

# PIXEES.FR USED EXTENSIONS #

Here are the used extensions, with a short notice about their usage.

## Enhanced content 

`class_code` : The Class'Code project plugin
`maths_ca` : The Maths'�a project plugin

### Within the content

`Widgets on Pages` : used to insert key-words cloud, category tree.
`http-syndication` : used to import other page orpost in a given page, alose used to offer content in raw rendering mode.

### Interfacing the content

`External Links` : To automatically manage external links
`Sociable` : used to allow to share content on social media (manual rendering put in ths footer.php).
`Subscribe to Comments Reloaded`:  enables commenters to sign up for e-mail notifications. 
`http-syndication` : to include, import and export post contents.
`iframe` : to include external HTML as frame.

### Content meta-data

`Add Meta Tags`: to automatically generates the metada of a post

## Category display

`Collapsing Categories` : used to display the newsletter category in an alternative way (generates warnings in WP_DEBUG mode).
`Simple Custom Post Order` : used to sort postes within a category.
`Category Order and Taxonomy Terms Order` : allows to sort category in a user defined order.
`WP Excerpt Generator` : automatically generates (thus erase and replace) all excerpts, used for category display post lists.
`Auto Post Thumbnail PRO` : automatically generates the thubmnails, used for slider.

## Complementary tools

`MailPoet Newsletters` : the newsletter generator using existing post.
`Fast Secure Contact Form` : the form generator for user feedback.
`Search Everything` : to enhance the search capabilities.
`Twitter Tweets Box` : to allow the inclusion of twitter
`New RoyalSlider` : to generate sliders with presentations (loaded as images) or post categories

## General site admin tools

`WP Cookie Banner` : to display a banner in order to comply with the EU Cookie Law. 
`Google Analyticator` : used to connect to google analytics and display results.
`Broken Link Checker` : checks broken links.
`Favicon by RealFaviconGenerator` : allows to prolerly add favicon.
`Enable Media Replace` : allows to replace media in the media library.
`Duplicate Post` : to initiate a new post by copy of another.
`WM Simple Captcha` : to include a captcha on inscription page.
`Google Language Translator` : used to connect to google translation to attempt to translate the site.

------------------------------------------------------------------------------------------------------

# MANUAL PATCHES #

* wp-content/plugins/sociable/sociable.php:		echo "</script><script type='text/javascript' src='https://apis.google.com/js/plusone.js'></script>";
  * http -> https
						
