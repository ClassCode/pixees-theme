<section>

  <article>
    <p>Rechercher une ressource en informatique ou mathématiques appliquées ? Solliciter une intervention ? Poser une question de curiosité scientifique ? Partager un sujet lié aux sciences du numérique ? Obtenir des réponses d’experts ou  être mis en relation avec des chercheurs ? Ce service de réponse aux questions est ouvert à toutes et tous : curieux de science, lycéens (pour vos TPEs), étudiants de CPGE (pour vos TIPEs), enseignant, étudiants, grand public, mais aussi aux chercheurs et ingénieurs en entreprises.</p>
  </article>
  
  <!--article>
    <h3>Formulaire de contact</h3>
    <a href="?page_id=2749" id="mail"></a>
    <p>Vous voulez poser une question, trouver une ressources ou un contact, parler avec des professionnels, donner une avis/une correction, proposer une ressources, prendre rendez-vous. Ce formulaire est pour vous. Il permet pour vous de structurer votre demande et pour nous de mieux y répondre… et on vous le promet il y a quelqu’un derrière l’ordi !</p>
  </article>              
  
  <article>
    <h3>Par vidéo conférence</h3>
    <a href="?page_id=2319" id="hangout"></a> 
    <p>Nous répondons à vos questions, échangeons en direct avec vous, par vidéo conférence !
    Notre bureau en ligne est ouvert les mercredis et jeudis de 14:00 à 17:00, sur <a href="mailto:pixees-accueil@inria.fr?subject=Je%20souhaite%20vous%20rencontrer%20sur%20hangout&amp;body=%20Quand%20cela%20possible:%20%0A%0A%20Ma%20question%20:%20%0A%0A%20Qui%20je%20suis%20%20:%20">rendez-vous</a>. Lancer la connexion en cliquant <a href="?page_id=2319">ICI</a> et d’entrer l’email <i>mecscicontact@gmail.fr</i>.<br/>
  </article!~-->
  
   <article style="background-color:#edf3f4;padding:10px 20px 20px 20px;margin:0px -20px 0px -20px;">
    <h3>Notre accueil en ligne </h3>
   <div align="center"><a style="font-weight:bold;font-size:24px" href="mailto:pixees-accueil@inria.fr?subject=Ma%20question%20%C3%A0%20propos%20de%20science%20du%20num%C3%A9rique&amp;body=%20Ma%20question%20:%20%0A%0A%20Qui%20je%20suis%20%20:">pixees-accueil@inria.fr</a></div>
    <ul id="contact-bar">
      <li><a href="mailto:pixees-accueil@inria.fr?subject=Ma%20question%20%C3%A0%20propos%20de%20science%20du%20num%C3%A9rique&amp;body=%20Ma%20question%20:%20%0A%0A%20Qui%20je%20suis%20%20:" title="pixees-accueil@inria.fr" id="mail"></a></li>
      <li><a target="_blank" href="https://twitter.com/intent/tweet?source=webclient&text=%23questiondesciencedunum%C3%A9rique+%40twitter_fr" title="@pixees_fr" id="hangout"></a></li>
      <!--li><a href="javascript:alert('+33 4 92 38 76 88');" title="+33 4 92 38 76 88" id="tel"></a></a></li-->
    </ul>
  </article>
  
  <article>
    <h3>La médiation Inria près de chez vous</h3>
    <p>Inria c’est une dizaine de sites répartis sur toute la France. Pour en savoir plus sur leurs activités et trouver les bons contacts près de chez vous <a target="_blank" href="https://pixees.fr/echanger-entre-nous/des-questions-des-propositions-les-bonnes-adresses-par-ici/">rendez-vous ici</a>.</br>
  <a target="_blank" href="https://pixees.fr/echanger-entre-nous/vos-contacts-mediation-scientifique-inria-au-national-pres-de-chez-vous-ou-en-ligne/"><img style="width:90%;margin:0 auto;" src="<?php echo get_site_url();?>/wp-content/themes/pixees-theme/_img/contacts-inria.png" "alt="Contacts Inria"/></a>

</p>
  </article>
  
</section>

