<div id="classcode-menu-bar">
  <nav id="classcode-menu" >
    <ul>
      <li><?php the_theme_context_link("page_id=".classCodeHomePageId(), "Accueil", true);?></li>
      <li><?php the_theme_context_link("page_id=8654", "Actualités", true);?></li>
      <li><?php the_theme_context_link("page_id=7995", "Documentations", true);?></li>
      <li><a href="<?php echo get_site_url(); ?>/classcode/accueil/#moocs">Formations</a></li>
      <li><a href="<?php echo get_site_url(); ?>/classcode/accueil/#meeting">Rencontres</a></li>
      <li><a href="<?php echo get_site_url(); ?>/classcode/accueil/#resources">Ressources</a></li>
      <li><a href="<?php echo get_site_url(); ?>/classcode/accueil/#classcode_footer">Aide</a></li>
    </ul>
  </nav>
</div>
