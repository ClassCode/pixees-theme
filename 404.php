<?php 
  // Redirection des pages du blog Class'Code
  {
    $blog_pages = array("la-formation", "mentions-legales", "news", "colloque-robotique-et-education", "lagenda-de-classcode", "experimentation-en-martinique", "experimentation-en-region-pays-de-la-loire", "experimentation-en-region-provence-alpes-cote-dazur", "le-printemps-de-classcode", "kit-de-presse", "ils-elles-parlent-de-classcode", "classcode-in-english", "classcode-ses-valeurs", "decouvrir-la-future-formation-classcode", "classcode-en-un-flyer", "classcode-qui-fait-quoi", "glossaire-des-termes-utilises-dans-classcode-2", "your-first-news");
    $blog_page = str_replace("/", "", substr($_SERVER['REQUEST_URI'], strlen("/classcode/")));
    if (in_array($blog_page, $blog_pages)) {
      echo '<script language="javascript">location.replace("https://classcode.fr/projet/'.$blog_page.'");</script>';
      echo 'La page recherchée est celle ci: <a href="https://classcode.fr/projet/'.$blog_page.'"</a>https://classcode.fr/projet/'.$blog_page.'</a>';
      exit(0);
    }
  }
?>

<?php get_header(); ?><div id="conteneur"><section id="contenu">

  <?php include(get_template_directory().'/_inc/site-404.php'); ?>

</section><?php get_sidebar(); ?></div><?php get_footer(); ?>
