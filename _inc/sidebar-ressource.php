<aside id="sidebar"> 

<?php
function the_theme_always_link($link, $title) {
   echo "<a href=\"".get_site_url()."?$link\">$title</a>";  
}
?>

  <section> 	 
    <h1>Ressources</h1> 
    <ul>
      <li><h4><?php the_theme_always_link("page_id=36", "À la une"); ?></h4></li>
      <li><h3>Zoom sur nos activités</h3></li>
      <li><h4><?php the_theme_always_link("s=débranché&orderby=relevance", "débranchées"); ?></h4></li>
      <li><h4><?php the_theme_always_link("s=scratch&orderby=relevance", "avec Scratch"); ?></h4></li>
      <li><h4><?php the_theme_always_link("s=thymio&orderby=relevance", "avec Thymio"); ?></h4></li>
      <li><h3><a href="#" onClick="openclose('item-thematique');"/>Thématique</a></h3></li>
      <li class="item-thematique"><h4><?php the_theme_always_link("cat=40", "Initiation aux algorithmes"); ?></h4></li>
      <li class="item-thematique"><h4><?php the_theme_always_link("cat=781", "Initiation à la programmation"); ?></h4></li>
      <li class="item-thematique"><h4><?php the_theme_always_link("cat=75", "Représenter les informations"); ?></h4></li>
      <li class="item-thematique"><h4><?php the_theme_always_link("cat=74", "Histoire de l´informatique"); ?></h4></li>
      <li class="item-thematique"><h4><?php the_theme_always_link("cat=42", "Numérique et société"); ?></h4></li>
      <li class="item-thematique"><h4><?php the_theme_always_link("cat=58", "Culture Générale"); ?></h4></li>
      <li class="item-thematique"><h4><?php the_theme_always_link("cat=84", "Métiers du numérique"); ?></h4></li>
      <li><h3><a href="#" onClick="openclose('item-publics');"/>Publics</a></h3></li>
      <li class="item-publics"><h4><?php the_theme_always_link("cat=98", "Professeurs des écoles"); ?></h4></li>
      <li class="item-publics"><h4><?php the_theme_always_link("cat=132", "Professeurs du secondaire"); ?></h4></li>
      <li class="item-publics"><h4><?php the_theme_always_link("cat=621", "Educateurs"); ?></h4></li>
      <li class="item-publics"><h4><?php the_theme_always_link("cat=150", "Parents"); ?></h4></li>
      <li class="item-publics"><h4><?php the_theme_always_link("cat=109", "Primaire"); ?></h4></li>
      <li class="item-publics"><h4><?php the_theme_always_link("cat=72", "Collège"); ?></h4></li>
      <li class="item-publics"><h4><?php the_theme_always_link("cat=91", "Lycée"); ?></h4></li>
      <li class="item-publics"><h4><?php the_theme_always_link("cat=96", "Enseignement supérieur"); ?></h4></li>
     </ul>
     <div id="line-sidebar"></div>
     <ul>
      <li><h3><?php the_theme_always_link("cat=608&alphabeticpage=a", "Lexique"); ?></h3></li>
    </ul>
  </section>
  
   <div id="line-sidebar"></div>

  <section>
   <ul>
     <li><h3><?php the_theme_always_link("page_id=2349", "Recherche détaillée"); ?></h3></li>
     <li><h3><?php the_theme_always_link("page_id=42", "Besoin d'aide ?"); ?></h3></li>
   </ul>
  </section>
  
</aside>
<script>
function openclose(cl) {  
  var els = document.getElementsByClassName(cl);
  for(var i = 0; i < els.length; i++) {
    var e = els[i];
    e.style.display = e.style.display == 'none' ? 'block' : 'none';
  }
}
openclose('item-zoom');
openclose('item-thematique');
openclose('item-publics');
</script>
