<div id="classcode_footer_logo">
  <img src="<?php the_theme_file('/_img/classcode_pictos/img-accueil-15.png');?>">
</div>
<div id="classcode_footer">
  <div id="classcode_footer_title">
    Le Bureau d'accueil
  </div>
  <div id="classcode_footer_content">
    Je ne sais pas bien ce que je cherche ou je ne trouve pas ? J'ai des id&eacute;es, des propositions, des questions, comment je fais ? J'aimerais participer ? La m&eacute;diation Inria &eacute;change avec vous par mail, t&eacute;l&eacute;phone ou visioconf&eacute;rence.
  </div>
  <div id="classcode_footer_mail">
    <a href="<?php echo cc_mailto_shortcode(array("subject" => "Ma question à propos de Class´Code")); ?>" >classcode-accueil@inria.fr</a>
  </div>
  <div id="class_code_footer_link">
    <a title="Nous contacter par email" id="classcode_footer_email" href="<?php echo cc_mailto_shortcode(array("subject" => "Ma question à propos de Class´Code")); ?>" ></a>
    <a title="Nous contacter au téléphonne" id="classcode_footer_phone" href="javascript:alert('+33 4 92 38 76 88');" title="+33 4 92 38 76 88" ></a>
    <a title="Consulter la foire aux questions" id="classcode_footer_doc" href="https://classcode.fr/accueil/documentation/" ></a>
  </div>
</div>
<div id="class_code_footer_bar">   
  <ul id="class_code_footer_bar_links">
    <li><a href="https://classcode.fr/credits-et-mentions-legales/">Crédits et mentions légales&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</a></li>
    <li><a href="https://classcode.fr/projet/classcode-qui-fait-quoi/" >Qui fait quoi dans Class´Code ?&nbsp;</a></li>
    <li><a href="https://classcode.fr/projet/" >Le site des partenaires du projet</a></li>
    <li><i><a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSfNckW9l5jdafm6gcRjcFfvQhgBHisAavyZ_hw9Q8U-2g4H5Q/viewform" >Je donne mon avis sur ce site&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></i></li>
  </ul>
</div>