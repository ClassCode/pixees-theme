#!/bin/bash

# This files contains the list of authorized IP 
file=/usr/share/wordpress/wp-content/uploads/userip_meta_field.txt

# These are the commands used to manage the remotics bridge
case "$1" in

  reboot ) # To be run after each reboot with root permission (use crontab).
    # Opens read and write access to these devices and $file directory, thymio and camera must be connected
    chmod -R a+rwx /dev/ttyACM* /dev/video* `dirname $file`
    # Upgrades the system packages
    apt-get upgrade -y
    # Starts the thymio and video service at the user level
    su - vthierry $0 start &> $0.log
    ;;

#
# More information, here:
# - In our case : the user login is vthierry and this script is in the /home/vthierry/hoomer.sh file
# The following line is to be added in the root crontab using the syntax, using the crontab -e command:
# @reboot sleep 60 && /home/vthierry/homer.sh reboot
#
# - The Thymio wifi access has been configured with https://www.thymio.org/fr:thymiosettingwireless on another computer before inserting the dongle in the system
#
# - For the IP filtering please contact us at classcode-accueil@inria.fr
#

  start ) # To be run after each reboot or failure with user permission.
    echo Starting homer at `date '+%D-%H:%M'`
    # Notice: echo "ALL ALL= NOPASSWD: /home/vthierry/bin/homer.sh" >> /etc/sudoers # must be done to allow sudoing homer.sh
    # Cleans older IP in the file
    $0 clean
    # Starts or restarts the aseba switch allowing remoter access of thymio /dev/ttyACM0 on port 33333
    killall asebaswitch 2> /dev/null
    nohup /usr/bin/asebaswitch -v -p 33333 "ser:device=/dev/ttyACM0" &
    # Starts or restarts the video streaming from /dev/video0 on port 8180
    killall cvlc 2> /dev/null
    nohup /usr/bin/cvlc v4l2:///dev/video0 --sout '#transcode{vcodec=theo,vb=800,scale=auto,acodec=vorb,ab=32,channels=1,samplerate=22100}:std{access=http{mime=video/ogg},mux=ogg,dst=:8180/}' &
    ;;

  open ) # Adds the IP to the authorization files with the date 
   shift
   echo "$1 #`date +%s`" >> $file
   ;;

  clean ) # Scans $file and remove IPs added more than one month ago
   mv -f $file $file~ 
   now="`date +%s`"
   cat $file~ | while read line
   do
     when="`echo $line | sed 's/.*#//'`"
     if [ $(($now - $when)) -lt $((3600 * 24 * 30)) ]
     then echo $line >> $file
     fi
   done
   ;;

  * ) # Displays this scrip usage if unknown option
   cat << EOD
  Usage: homer.sh (start|open $IP)
   Manages the https://pixees.fr/remotics remote thymio manipulation.
  Options:
   start     : Starts the aseba switch for Thymio remote control and the video stream.
   open <IP> : Adds an $IP in order to allow remote access to this machine.
EOD
   ;;
esac


