<aside id="sidebar">

 <section> 	  
   <h1>Formations</h1>
   <ul>
     <li><h4><?php the_theme_context_link("page_id=9822", "À la une"); ?></h4></li>
   </ul>
 </section>
  
<div id="line-sidebar"></div>

  <section> 	 
    <h3>Formations Class´Code</h3>
   <ul>
     <li><h4><?php the_theme_context_link("p=9960", "#1 Programmation créative"); ?></h4></li>
     <li><h4><?php the_theme_context_link("p=9963", "#2 Codage de l'information"); ?></h4></li>
     <li><h4><?php the_theme_context_link("p=10311", "#3 Robotique ludique"); ?></h4></li>
     <li><h4><?php the_theme_context_link("p=11217", "#4 Comprendre les réseaux"); ?></h4></li>
     <li><h4><?php the_theme_context_link("p=11611", "#5 Mener un projet avec les enfants"); ?></h4></li>
    </ul>
 </section>
  
<div id="line-sidebar"></div>

  <section> 	 
    <ul>
      <li><h3><?php the_theme_context_link("page_id=42", "Nous contacter"); ?></h3></li>
    </ul>
  </section>

</aside>
