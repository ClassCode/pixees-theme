<div id="loginFooter">
<?php 
   $once_gain = isset($_REQUEST['action']) && in_array($_REQUEST['action'], array('register','lostpassword','rp','resetpass'));
  if($once_gain){
?>
Pour pouvoir b&eacute;n&eacute;ficier de toutes les fonctionalit&eacute;s de Class'Code, notamment en ce qui concerne les formations, il est n&eacute;cessaire de s'inscrire. Le traitement et stockage des données a été déclaré au Correspondant Informatique et Liberté Inria.
<?php
  }else{
?>
Le saviez vous ? En cochant "Se souvenir de moi" vous n'aurez pas à rentrer votre mot de passe à chaque visite. Les informations permettant de vous identifier seront stockées sur votre ordinateur, pas de risque que l'on utilise votre mot de passe ! Les informations personnelles font l'objet d'un dépôt à la CNIL.
<?php  
  }
?>
<script>
<?php
  $lostpwd = $once_gain ? "" : "<div style='float:right;'><a href='".wp_lostpassword_url(get_permalink())."'>Mot de passe oublié ?</a>";
?>  
  document.getElementById("backtoblog").innerHTML = "<a href='https://classcode.fr/'>&larr; Retour sur Class´Code</a><?php echo $lostpwd; ?>";

<?php
if ($_REQUEST['action'] == 'register') 
  echo 'document.getElementById("backtoblog").innerHTML +=  "<div style=\'float:right;\'><a target=\'_blank\' href=\'https://www.youtube.com/watch?v=zLYAfcP1esk\'><img src=\''.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_pictos/i-tooltip-aide-gray.png\'/> Regarder le tutoriel</a></div>"';
if ($_REQUEST['action'] == 'lostpassword') 
  echo 'document.getElementById("backtoblog").innerHTML +=  "<div style=\'float:right;\'><a target=\'_blank\' href=\'https://www.youtube.com/watch?v=qnpiiy14XPQ\'><img src=\''.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_pictos/i-tooltip-aide-gray.png\'/> Regarder le tutoriel</a></div>"';

?>

  if (document.getElementById("loginform") != undefined)
    document.getElementById("loginform").innerHTML = document.getElementById("loginform").innerHTML.replace(/Nom d’utilisateur ou adresse e-mail/, 'Identifiant ou e-mail');

  if(document.getElementById("login").getElementsByClassName('message')[0] != undefined)
    document.getElementById("login").getElementsByClassName('message')[0].innerHTML = "<h4 style='font-size:16px;color:black;'>Vous venez de demander un mot de passe pour Class´Code.<ul style='padding-left:20px'><li>Vous allez recevoir un email qui va vous permettre de le choisir en cliquant (une fois) sur le lien proposé.</li><li>Note: <i>bien cliquer sur ``réinitialiser le mot de passe´´</i>.</li><li>Vous pourrez alors vous connecter avec votre identifiant et ce mot de passe.</li></ul>En cas de problème, contacter <a href='https://classcode.fr/accueil/aide?cc_mailer_shortcode_to=classcode-accueil%40inria.fr&cc_mailer_shortcode_who=Bureau%20d%C2%B4Accueil%20de%20Class%C2%B4Code&cc_mailer_shortcode_subject=J%C2%B4ai%20un%20souci%20pour%20me%20connecter%20%C3%A0%20la%20plateforme%20https%3A%2F%2Fclasscode.fr&cc_mailer_shortcode_body=&body_completion=true#aideAction2'>classcode-accueil@inria.fr</a>, nous sommes à votre disposition.<br/>À bientôt sur Class´Code!</h4>";
<?php
  if($_REQUEST['action'] == 'rp'){
?>  
    document.getElementById("header").innerHTML = document.getElementById("header").innerHTML + "<h4 style='padding:10px;font-size:16px;color:black;'>Voici votre mot de passe, <br/>- vous pouvez le changer si vous souhaitez un mot de passe plus lisible, <br/>- surtout: copiez le bien en lieu sûr dans tous les cas, avant de taper ``Réinitialiser ..´´<br/>Vous pouvez alors vous connecter.</h4>";
<?php  
  }
?>

<?php

if ($_REQUEST['action'] == 'register') 
  echo '
var code_premium_div = document.createElement("div");
code_premium_div.innerHTML =  "<div style=\"padding:10px 20px 10px 20px;color:#266D83;font-size:14px;font-weight:bold;height:80px;\"><img style=\"height:40px;\" src=\"'.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-openclassrooms.png\"/> Pour me former rapidement, après avoir renseigné les champs ci-dessous, je <a id=\"code_premium_link\" target=\"blank\" onClick=\"update_code_premium();\" href=\"\">récupère mon Code Premium</a> pour bénéficier d´un accès illimité à OpenClassrooms pendant 1 mois !<a target=\"blank\" href=\"https://openclassrooms.com/premium\">*</a></div><div style=\"padding:10px 20px 10px 20px;color:#266D83;font-size:14px;font-weight:bold;\"><img style=\"height:20px;\" src=\"'.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-openclassrooms.png\"/> Si je suis déjà incrit.e sur <a target=\"_blank\" href=\"https://openclassrooms.com\">OpenClassrooms</a> pas besoin de m´inscrire de nouveau ici, il suffit de me <a title=\"Attention, cette fonctionnalité est encore en test, nous contacter si il y a un souci !\" href=\"'.get_site_url().'/wp-content/plugins/class_code/oc_api\">connecter avec mon compte OpenClassrooms</a>.</div>";
document.getElementById("login").insertBefore(code_premium_div, document.getElementById("registerform"));

function update_code_premium() {
  document.getElementById("code_premium_link").href = "'.get_site_url().'/wp-content/plugins/class_code/code-premium/index.php?who="+document.getElementById("user_email").value+"&ohw="+((document.getElementById("user_email").value.length * 104729) % 10000);
}

';
else 
  echo '
var you_can_register = document.createElement("div");
you_can_register.innerHTML =  "<div style=\"padding:20px 20px 20px 40px;color:#266D83;font-size:14px;font-weight:bold;\">Première visite ? Cliquez <a href=\"'.get_site_url().'/wp-login.php?action=register\">ici pour s´incrire</a>!</div><div style=\"padding:10px 20px 10px 40px;color:#266D83;font-size:14px;font-weight:bold;\"><img style=\"height:20px;\" src=\"'.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-openclassrooms.png\"/> Si je suis déjà incrit.e sur <a target=\"_blank\" href=\"https://openclassrooms.com\">OpenClassrooms</a>, il suffit de me <a title=\"Attention si je je suis déjà inscrit aussi sur cette plateforme, cela me connectera avec un autre compte\" href=\"'.get_site_url().'/wp-content/plugins/class_code/oc_api\">connecter avec mon compte OpenClassrooms</a>.</div>";
if (document.getElementById("loginform") != undefined)
  document.getElementById("login").insertBefore(you_can_register, document.getElementById("loginform"));
';
?>

</script>


</div>