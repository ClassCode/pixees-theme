<?php
  /** Implements the possibility to show a QRcode corresponding to a post
   * Typical Usage:<pre>
   * // Adds an icon in the share line (e.g. social share) of post
   * if (function_exists("do_show_post_QRcode")) { echo do_show_post_QRcode; }
   * </pre>
   * @param $size The icon size, either 16, 32 or 64. Default is 32.
   */
function do_show_post_QRcode($size = 32) {
  echo '<a style="display:table-cell;" title="QR-code de cette page" target="QRcode" href="'.get_template_directory_uri().'/show_post_QRcode/show.php"><img src="'.get_template_directory_uri().'/show_post_QRcode/show_post_QRcode_icon_'.$size.'x'.$size.'.png" alt="show post QRcode"/></a>';
}
?>