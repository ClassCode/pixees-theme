<?php

add_theme_support( 'post-thumbnails' ); 
// Default text-width
if (!isset($content_width)) {
  $content_width = 750;
}

// Constraints the display of all posts without pager
update_option('posts_per_page', 1000);
update_option('comments_per_page', 1000);

// Registers and tune the menu and widget admin
function the_theme_widgets_init() {
  register_sidebar(array(
			 'name' => 'Home right sidebar',
			 'id' => 'home_right_1',
			 'before_widget' => '<div>',
			 'after_widget' => '</div>',
			 'before_title' => '<h2 class="rounded">',
			 'after_title' => '</h2>'));
}
add_action('widgets_init', 'the_theme_widgets_init', 999);

// Simplifies the menu edition according to what is in $GLOBALS['menu'] and $GLOBALS['submenu']
function the_theme_admin_init() {
  global $submenu;
  if (!get_option('pixees-theme/root-mode', false)) {
    remove_submenu_page('index.php', 'update-core.php');
    remove_menu_page('themes.php');
    {
      // Remove_submenu_page('themes.php', 'customize.php');
      if (isset($submenu['themes.php'][6])) unset($submenu['themes.php'][6]); 
      remove_submenu_page('themes.php', 'nav-menus.php');
    }
    remove_submenu_page('wysija_campaigns', 'wysija_statistics');
    remove_submenu_page('wysija_campaigns', 'wysija_config');
    remove_submenu_page('wysija_campaigns', 'wysija_premium');
    remove_menu_page('plugins.php');
    remove_submenu_page('tools.php', 'tools.php');
    remove_submenu_page('tools.php', 'import.php');
    remove_menu_page('options-general.php');
    remove_menu_page('slug_for_twitter_box');
    remove_menu_page('stcr_manage_subscriptions');
    remove_menu_page('google-analyticator');
    remove_menu_page('bws_panel');
    remove_menu_page('widgets-on-pages');
    remove_menu_page('new_royalslider');
    remove_menu_page('captcha.php');
    remove_menu_page('options-general.php?page=sociable_select');
    // Close Buddypress et la configuration
    remove_menu_page('bp-activity');
    remove_menu_page('bp-groups');
    remove_menu_page('gmw-add-ons');
    remove_submenu_page('tools.php', 'bp-tools');
    remove_submenu_page('users.php', 'bp-signups');
    remove_submenu_page('users.php', 'bp-profile-setup');
  }
  if (!current_user_can('manage_options')) {
    // remove_submenu_page('edit.php', 'install_plugins');
    if (isset($submenu['edit.php']))
      foreach($submenu['edit.php'] as $index => $data)
	if ($data[0] == 'Ordre taxonomique')
	  unset($submenu['edit.php'][$index]); 
  }
}
add_action('admin_menu', 'the_theme_admin_init', 999);

function the_admin_bar_init($wp_admin_bar) {
  if (!get_option('pixees-theme/root-mode', false))
    foreach (array(     	
		   // Cancels the WP menu
		   'wp-logo', 'about', 'wp-logo-external', 'wporg', 'documentation', 'support-forums', 'feedback',
		   // Cancels pixees options and customization
		   'themes', 'widgets', 'menus', 'customize') as $item)
      $wp_admin_bar->remove_node($item);  
  // Add a short-cut for draft edition
  $wp_admin_bar->add_node(array(
				'id' => 'edit_drafts',
				'title' => 'Brouillons',
				'href' => get_site_url().'/wp-admin/edit.php?post_status=draft',
				'parent' => false // 'edit.php'
				));

}
add_action('admin_bar_menu', 'the_admin_bar_init', 999);

// add_action('admin_notices', function() { global $menu, $submenu;  echo '<pre>'.print_r($submenu, true).'<hr>'.print_r($menu, true).'</pre>'; });

// Removes the display status of the Toolbar for the front side of the website
if (!current_user_can('manage_options')) {
  show_admin_bar(false);
}

// Displays an alert on the page
function the_theme_alert($message = "") {
 echo "<div style=\"background-color:yellow;color:red;display:block;witdh:100%;padding:50px;\">Oh ! Il y a un bug dans le theme: « $message »</div>";
}

// Adds short code to import theme contents
function the_theme_import_shortcode($atts, $content) {
  if (isset($atts['inc'])) {
    $file = get_template_directory().'/_inc/'.$atts['inc'].'.php';
    if (file_exists($file)) {
      ob_start();
      include($file);
      $contents = ob_get_contents();
      ob_end_clean();
      return $contents;
    } else
      return "[pixees inc=\"".$atts['inc']."\"] error: _inc/".$atts['inc'].".php file not found";
  } else
    return "[pixees error='the `inc` parameter is not defined, thus nothing to import']";
}
add_shortcode('pixees', 'the_theme_import_shortcode');

// Echoes a theme file location
function the_theme_file($name) {
  echo get_template_directory_uri().$name;
}

// Simplifies comment fields : only email, no name, no url.
function comment_simplified($fields)
{
  if(isset($fields['author'])) unset($fields['author']);
  if(isset($fields['url'])) unset($fields['url']);
  return $fields;
}
add_filter('comment_form_default_fields', 'comment_simplified');

// Adding two new status to the workflow
function custom_post_status() {
  register_post_status('obsolete', array(
					 'label'                     => _x( 'Obsolète', 'post' ),
					 'public'                    => true,
					 'exclude_from_search'       => true,
					 'show_in_admin_all_list'    => true,
					 'show_in_admin_status_list' => true,
					 'label_count'               => _n_noop('Obsolète <span class="count">(%s)</span>', 'Obsolète <span class="count">(%s)</span>'),
					 ));
  register_post_status('canceled', array(
					 'label'                     => _x( 'Abandonné', 'post' ),
					 'public'                    => false,
					 'exclude_from_search'       => true,
					 'show_in_admin_all_list'    => true,
					 'show_in_admin_status_list' => true,
					 'label_count'               => _n_noop('Abandonné <span class="count">(%s)</span>', 'Abandonné <span class="count">(%s)</span>' ),
					 ));

}
add_action('init', 'custom_post_status', 0);
// Adding the related metabox to manage such status
function custom_post_status_meta_box_html($post) {
  $status_ids = array(
		      //'auto-draft' => "En création", // generates a bug (kill the title)
		      'draft' => "Brouillon", 
		      'pending' => 'En attente de relecture',
		      'publish' => "Publié", 
		      'obsolete' => "Obsolète", 
		      'canceled' => "Abandonné", 
		      'private' => "Privé",  
		      //'inherit' => "Attachement", // not applicable
		      //'trash' => "Dans la poubelle", // generates a a bug do not use here
		      );
  $status = get_post_status($post);
  echo '<div><select name="custom_post_status" id="custom_post_status"><option value="none"></option>';
  foreach($status_ids as $id => $label)
    echo "<option ".($id == $status ? "selected='selected'" : "")." value='$id'>$label</option>";
  echo '</select></div><div><input  type="submit" name="save_custom_post_status" id="save-post" value="Enregistrer" class="button" /></div><div class="clear"></div>';
}
function custom_post_status_meta_box() {
  // Adds to all screens
  add_meta_box(
	       'custom_post_status_meta_box',
	       'Status du post',
	       'custom_post_status_meta_box_html',
	       'post',
	       'side',
	       'core'
	       );
  add_meta_box(
	       'custom_post_status_meta_box',
	       'Status du post',
	       'custom_post_status_meta_box_html',
	       'page',
	       'side',
	       'core'
	       );
}
add_action('add_meta_boxes', 'custom_post_status_meta_box');
function custom_post_status_meta_box_data($post_id) {
  if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) ||
      (!current_user_can('edit_post', $post_id))) {
    return;
  }
  if (isset($_POST['save_custom_post_status']) && isset($_POST['custom_post_status']) && $_POST['custom_post_status'] != 'none') {
    remove_action('save_post', 'custom_post_status_meta_box_data');
    custom_post_status_set($post_id, $_POST['custom_post_status']);
    add_action('save_post', 'custom_post_status_meta_box_data');
  }
}
function custom_post_status_set($post_id, $post_status) {
  $post = array();
  $post['ID'] = $post_id;
  $post['post_status'] = $post_status;
  $status = get_post_status($post_id);
  if ($status != $post_status)
    wp_update_post($post);
}
add_action('save_post', 'custom_post_status_meta_box_data');

// Interfaces with the media gallery to use image title and description in gallery
function add_populate_img_meta($post_id)
{
  // Gets EXIF data from the attachment file
  $exif = @exif_read_data(get_attached_file($post_id));
  $exif_title = isset($exif['DocumentName']) ? wp_slash(wp_strip_all_tags($exif['DocumentName'])) : "";
  $exif_content = isset($exif['ImageDescription']) ? wp_slash(wp_strip_all_tags($exif['ImageDescription'])) : "";
  
  // Updates the title and caption
  wp_update_post(array('ID' => $post_id, 
		       'post_title' => $exif_title, 
		       'post_excerpt' => $exif_content, 'post_content' => $exif_content));
  update_post_meta($post_id, '_wp_attachment_image_alt', $exif_content);
}
add_filter('add_attachment', 'add_populate_img_meta');

// Customizes the login page
// Ref: https://codex.wordpress.org/Customizing_the_Login_Form
function pixees_enqueue_scripts() {
  $_REQUEST['context'] = 'presentation';
  include_once(get_template_directory().'/_inc/display-functions.php');
  if($GLOBALS['classcode_v2_is_online'] == true){
    wp_enqueue_style('custom-login', get_template_directory_uri().'/../../plugins/class_code_v2/assets/css/style.css');
  }else{
    wp_enqueue_style('custom-login', get_template_directory_uri() . '/classcode.css' );  
  }
}
add_action('login_enqueue_scripts', 'pixees_enqueue_scripts' );

function pixees_custom_login_message() {    
  ob_start();
  echo '<header id="header">';
  if($GLOBALS['classcode_v2_is_online'] == true){
    include(get_template_directory().'/_inc/classcode-login-banner-v2.php');  
  }else{
    include(get_template_directory().'/_inc/classcode-login-banner.php');  
  }
  echo '</header>';
  $html = ob_get_contents();
  ob_end_clean();  
  return $html;
}
add_action('login_message', 'pixees_custom_login_message');

function pixees_custom_login_footer() {
  if($GLOBALS['classcode_v2_is_online'] == true){
    include(get_template_directory().'/_inc/classcode-login-footer-v2.php');  
  }else{
    include(get_template_directory().'/_inc/classcode-login-footer.php');
  }
  
  
}
add_action('login_footer', 'pixees_custom_login_footer');

function pixees_custom_login_redirect($redirect_to, $request, $user) {
  if (isset($_REQUEST['redirect_to'])) {
    $redirect_to = $_REQUEST['redirect_to'];
    return $redirect_to;
  }  
  if (is_array($user->roles))
    if (in_array('administrator', $user->roles) || in_array('editor', $user->roles)) 
      return admin_url();  
  return get_site_url().'/classcode/accueil';
}
add_filter('login_redirect', 'pixees_custom_login_redirect', 10, 3);

// Displays the backup Class'Code page
add_filter('request', function ($request) {  
    if (isset($_REQUEST['class_code']) || isset($_REQUEST['cc'])) {
      header('Location: https://classcode.fr/?page_id=6064');
      exit(0);
    } else
      return $request;
  }, 999, 1);

// Manages the order in post slider
add_filter('new_royalslider_posts_slider_query_args', function($args, $index) {
    $args['order'] = 'ASC';
    $args['orderby'] = 'menu_order';
    return $args;
  }, 10, 2);

// Adds the theme management tools
include_once(get_template_directory().'/_inc/admin-menu.php');

//ClassCode
//redirect after successful password reset
function classcode_lost_password_redirect() {
    wp_redirect(site_url( "/wp-login.php?redirect_to=".urlencode(site_url("/classcode-v2/editer-votre-profile/" )) ));         
    exit;
}
add_action('after_password_reset', 'classcode_lost_password_redirect');

function retrieve_password_message_filter($message, $key, $user_login, $user_data) {
  $message = 'Bonjour ``'.$user_login.'´´,

 Vous semblez avoir besoin de renouveller votre mot de passe sur https://classcode.fr, voici comment faire:
   (si ce n´est pas le cas il suffit de ne pas tenir compte de ce mail)

 1/ Cliquer sur ce lien : '.network_site_url("wp-login.php?action=rp&key=$key&login=".rawurlencode($user_login), 'login').'
  (ou copiez le dans votre navigateur).
 2/ Vous y aurez un nouveau de passe qu´il faut précieusement copier avant d´appuyer sur «Réinitialiser le mot de passe».
 3/ Vous pourrez alors vous connecter avec votre identifiant: '.$user_login.' et le nouveau mot de passe.

 Vous pouvez aussi regarder ce tutoriel : https://www.youtube.com/watch?v=qnpiiy14XPQ
 Et ne pas hésiter à nous contacter classcode-accueil@inria.fr si souci.

 À bientôt sur Class´Code !';

  return $message;
}
add_filter ( 'retrieve_password_message', 'retrieve_password_message_filter', 10, 4);

//allow comments on meeting to registration
add_filter('frm_validate_field_entry', 'my_custom_validation', 8, 3);
function my_custom_validation($errors, $posted_field, $posted_value){
  $_POST['frm_wp_post']['=comment_status'] = 'open';
  return $errors;
}
//redirect after formidableupdate to meeting list
function frm_meeting_list_redirect(){
  wp_redirect( home_url()."/classcode/les-reunions-dont-je-suis-lorganisateur/" );
  exit;
}
add_action( 'frm_after_update_entry','frm_meeting_list_redirect' );
add_action( 'frm_after_create_entry','frm_meeting_list_redirect' );

function classcode_display_currentUser_posts( $atts ){
  if ( is_user_logged_in() ):
    global $current_user;
    get_currentuserinfo();
    $author_query = array('posts_per_page' => '-1','author' => $current_user->ID);
    $author_posts = new WP_Query($author_query);
    $postList = "<ul>";
    while($author_posts->have_posts()) : $author_posts->the_post();
      $postList.= "<li><a href='".get_permalink()."' title='".get_the_title()."'>".get_the_title()."</a></li>";    
    endwhile;
    $postList.= "</ul>";
  else :
    $postList = "Veuillez vous connecter (donc vous inscrire) à cette plateforme pour continuer."    ;
  endif;
  return $postList;
}
add_shortcode( 'classCode_display_posts', 'classcode_display_currentUser_posts' );

function echo_help_tooltip($helpText, $picto_color = "white", $helpLink = false) {
  
  echo '<div class="helptooltip" >';
  if ($helpLink) {
  echo '<a target="_blank" href="'.$helpLink.'"><img class="helpImg" alt="helpIcone" src="'.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_pictos/'.($picto_color == 'gray' ? 'i-tooltip-aide-gray.png' : 'i-tooltip-aide.png').'" ></a>';
    echo '<span class="helptooltiptext helptooltip-top"><a href="'.$helpLink.'">'.$helpText.'</a></span>';
  } else {
  echo '<img class="helpImg" alt="helpIcone" src="'.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_pictos/'.($picto_color == 'gray' ? 'i-tooltip-aide-gray.png' : 'i-tooltip-aide.png').'" >';
    echo '<span class="helptooltiptext helptooltip-top">'.$helpText.'</span>';
  }
  echo '</div>';  
} 

/* utile pour le tableau des rencontres, pourrait ne pas être chargé sur toutes les pages */
function tablesorter_js() {  
  wp_enqueue_script( 'tablesorter',get_template_directory_uri() . '/tablesorter/jquery.tablesorter.min.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'tablesorter_js' );

//fonction pour supprimer des accents d'une chaine (utile pour comparer des structures)
function suppr_accents($str, $encoding='utf-8')
{
  // transformer les caractères accentués en entités HTML
  $str = htmlentities($str, ENT_NOQUOTES, $encoding);

  // remplacer les entités HTML pour avoir juste le premier caractères non accentués
  // Exemple : "&ecute;" => "e", "&Ecute;" => "E", "Ã " => "a" ...
  $str = preg_replace('#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str);

  // Remplacer les ligatures tel que : Œ, Æ ...
  // Exemple "Å“" => "oe"
  $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
  // Supprimer tout le reste
  $str = preg_replace('#&[^;]+;#', '', $str);

  return $str;
}// search filter

// Excludes classcode categories from search
function exclude_category_from_search($query)
{
  if ($query->is_search) {
    $query->set('post_type', array('post'));
    $query->set('category__not_in', array(953,955,956,957,958));
  }
  return $query;
}
add_filter('pre_get_posts', 'exclude_category_from_search');

// Allows severals mime file types to be uploaded in the network; 
function custom_upload_mimes ( $existing_mimes=array() ) {    
  $existing_mimes['svg'] = 'image/svg+xml';  
  $existing_mimes['txt'] = 'text/plain';
  // Adds additonal extensions if needed
  // $existing_mimes['XXXXX'] = 'mime/type';
  return $existing_mimes; 
} 
add_filter('upload_mimes', 'custom_upload_mimes');

//Pour compatibilité php7 où split disparait
if (!function_exists('split')) {
   function split($pattern, $string) { return explode($pattern, $string); };
}

//pour la customisation du css du dashboard
function admin_css() {

$admin_handle = 'admin_css';
$admin_stylesheet = get_template_directory_uri() . '/admin.css';

wp_enqueue_style( $admin_handle, $admin_stylesheet );
}
add_action('admin_print_styles', 'admin_css', 11 );

?>