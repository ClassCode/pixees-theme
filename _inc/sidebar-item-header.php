<section>
  <h3>Une communauté scientifique à votre écoute</h3>
  <p><a href="?page_id=42" id="pixees-contact"></a></p>      
  <p>Je ne sais pas bien ce que je cherche ou je ne le trouve pas&nbsp;? J'ai des idées, des propositions, des questions, comment je fais&nbsp;?  J'aimerais participer&nbsp;? La médiation Inria échange avec vous par mail, téléphone ou visio conférence.</p>
  <ul id="contact-bar">
    <!---li><a href="javascript:alert('+33 492687688');" title="+33 4 92 68 76 88" id="tel"></a></li-->
    <li><a href="mailto:pixees-accueil@inria.fr?subject=Ma%20question%20%C3%A0%20propos%20de%20science%20du%20num%C3%A9rique&amp;body=%20Ma%20question%20:%20%0A%0A%20Qui%20je%20suis%20%20:" title="pixees-accueil@inria.fr" id="mail"></a></li>
    <li><a href="?page_id=3122" title="@pixees_fr" id="hangout"></a></li>
  </ul>
</section>
