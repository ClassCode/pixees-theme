<aside id="sidebar">
               
  <?php include(get_template_directory().'/_inc/sidebar-item-header.php'); ?>
  
  <div id="line-sidebar"></div>

  <?php include(get_template_directory().'/_inc/sidebar-item-gazette.php'); ?>
  
  <div id="line-sidebar"></div>

  <section> 	 
    <ul> 
      <li><h3>À propos de pixees.fr</h3></li>
      <li><h4><?php the_theme_context_link("page_id=1980", "Partenaires"); ?></h4></li>
      <li><h4><?php the_theme_context_link("page_id=2394", "Présentation"); ?></h4></li>
      <li><h4><?php the_theme_context_link("page_id=2995", "Mentions légales"); ?></h4></li>
      <li><h4><?php the_theme_context_link("page_id=2989", "Licence et copyright"); ?></h4></li>
      <li><h4><?php the_theme_context_link("page_id=3387", "Crédits"); ?></h4></li>
    </ul>
  </section>

  <?php include(get_template_directory().'/_inc/sidebar-item-amis.php'); ?>

</aside>
