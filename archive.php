<?php 

include_once(get_template_directory().'/_inc/display-functions.php');

$raw = isset($_REQUEST['httpsyndication']);

// Manages the archive context
{
  function get_theme_category_context() {
    $root_category_to_context = root_category_to_context();
    $cat = get_category(get_query_var('cat'));
    while ($cat->parent != 0)
      $cat = get_category($cat->parent);
    return isset($root_category_to_context[$cat->slug]) ? $root_category_to_context[$cat->slug] : "ressource";
  }
  
  $_REQUEST["context"] = is_category() ? get_theme_category_context() : "ressource";
}

if (!$raw) { 
  // Echoes title and excerpt
  function the_theme_archive_title() {
    if (is_category() && !isset($_REQUEST["alphabeticpage"])) {
      $cat = get_category(get_query_var('cat'));
      $title = $cat->name;
      while ($cat->parent != 0) {
	$cat = get_category($cat->parent);
	$title = $cat->name." <b>»</b> ".$title;
      }
      echo "Catégorie : ".$title;
    } else if (is_tag()) {
      echo "Mot clé : "; single_tag_title();
    } else if (is_author()) {
      echo "Contenus créés par ".get_the_author();
    } else if (is_day() || is_month() || is_year()) {
      echo "Archive de ".(is_year() ? get_the_date('Y') : is_month() ? get_the_date('m YY') :  get_the_date());
    }
  }
  function the_theme_archive_excerpt() {
    if (is_author() && get_the_author_meta('description')) {
      the_author_meta('description');
    }
  }

  get_header(); 

  echo "<div id=\"conteneur\"><section id=\"contenu\">";
  echo "<div class=\"".$_REQUEST["context"]."\" style=\"margin:20px;\"><h1>"; the_theme_archive_title(); echo "</h1>"; the_theme_archive_excerpt(); echo "</div>\n";
}

the_theme_posts(false, true, false);
    
if (!$raw) { 
  echo "</section>"; get_sidebar(); echo "</div>"; get_footer(); 
}

?>
