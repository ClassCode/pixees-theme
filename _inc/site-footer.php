<div id="interline"></div>

<footer id="footer">
<?php if ($_REQUEST['context'] == 'classcode') {
        include(get_template_directory().'/_inc/classcode-footer.php'); 
      }else{ ?>  
  <div id="footer-logo">
     <img src="<?php the_theme_file('/_img/Logo-pixees-footer.png');?>" width="177" height="130" alt=""/><br/>
    <ul>
      <li><a href="?page_id=2609"><img src="<?php the_theme_file('/_img/fr.png');?>" width="32" height="21" alt="en français"/></a></li>
      <li><a href="?page_id=2420"><img src="<?php the_theme_file('/_img/uk.png');?>" width="32" height="21" alt="in English"/></a></li>
      <li><a href="?page_id=2466"><img src="<?php the_theme_file('/_img/es.png');?>" width="32" height="21" alt="en Español"/></a></li>
      <li><a href="?page_id=2560"><img src="<?php the_theme_file('/_img/all.png');?>" width="32" height="21" alt="im Deutsch"/></a></li>
      <li><a href="?page_id=2486"><img src="<?php the_theme_file('/_img/it.png');?>" width="32" height="21" alt="en Italiano"/></a></li>
    </ul>
  </div>
    
  <div id="footer-menu">
    <ul>
      <li><img src="<?php the_theme_file('/_img/pixees-actu.png');?>" width="48" height="48" alt=""/></li>
        <li><h6><a href="?page_id=2697">Actualité</a></h6></li>
        <li><a href="?page_id=2890">> Archives</a></li>
        <li><a href="?page_id=3122">> Twitter</a></li>
        <li><a href="?page_id=44">> Projets</a></li>
    </ul>
  </div>
    
  <div id="footer-menu">
    <ul>
      <li><img src="<?php the_theme_file('/_img/pixees-ressources.png');?>" width="48" height="48" alt=""/></li>
        <li><h6><a href="?page_id=36">Ressources</a></h6></li>
        <li><a href="?page_id=2584">> Thématique</a></li>
        <li><a href="?page_id=2585">> Public</a></li>
        <li><a href="?page_id=2535">> Format</a></li>
        <li><a href="?page_id=2349">> Recherche</a></li>
    </ul>
  </div>
 
  <div id="footer-menu">
    <ul>
      <li><img src="<?php the_theme_file('/_img/pixees-interventions.png');?>" width="48" height="48" alt=""/></li>
        <li><h6><a href="?page_id=40">Interventions</a></h6></li>
        <li><a href="?cat=483">> Conférences</a></li>
        <li><a href="?cat=491">> Ateliers</a></li>
        <li><a href="?cat=491">> Formations</a></li>
        <li><a href="?cat=485">> Débats</a></li>
        <li><a href="?page_id=2329">> Exemples</a></li>
    </ul>
  </div>
 
  <div id="footer-menu">
    <ul>
      <li><img src="<?php the_theme_file('/_img/pixees-contacts.png');?>" width="48" height="48" alt=""/></li>
        <li><h6><a href="?page_id=42">Contacts</a></h6></li>
        <li><a href="?page_id=2319">> En ligne</a></li>
        <li><a href="?page_id=2749">> Message</a></li>
        <li><a href="?page_id=2781">> Les TPE/TIPE</a></li>
    </ul>
  </div>
 
  <div id="footer-menu">
    <ul>
      <li><img src="<?php the_theme_file('/_img/pixees-participez.png');?>" width="48" height="48" alt=""/></li>
        <li><h6>Divers</h6></li>
        <li><a href="?page_id=1980">> Partenaires</a></li>
        <li><a href="?page_id=2394">> Présentation</a></li>
        <li><a href="?page_id=2995">> Mentions</a></li>
        <li><a href="?page_id=2989">> Licence</a></li>
        <li><a href="?page_id=3387">> Crédits</a></li>
        <li><a href="<?php echo get_site_url(); ?>/wp-admin">> Back office</a></li>
    </ul>
  </div>
<?php } ?>
</footer>

