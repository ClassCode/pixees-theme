<?php
  /* Usage: php html.php
   * - Ce script permet de generer la Web présentation de 'projeter son projet.odp.pptx' 
   *  -- avec un export HTML à faire à la main dans le répertoire de nom ./html avec les options 'Standard HTML format', 'Create title page' puis 'PNG', 'High 1024 x 768' resolution.
   *  -- en y exportant aussi la version PDF et éditable
   * - Il génère un page ./html/index.html et des pages ./html/page*.html avec la présentation
   */
{
  // Copie le fichier éditable
  system('cp "projeter son projet.odp.pptx" ./html');

  // Applies an array of preg pattern -> remplacement from a source file to a target file.
  function file_pregs_replace($patterns, $remplacements, $source, $target = false) {
    $text = file_get_contents($source);
    foreach($patterns as $index => $pattern)
      $text = preg_replace($patterns[$index], $remplacements[$index], $text);
    file_put_contents(($target ? $target : $source), $text);
  }
 // Builds the menu bar form the file source and its split
  function get_page_from_source($source) {
    $pages = array(array("5", "Avant", "", ""),
		   array("6", "Avant", "Explorer", "Clés"),
		   array("7", "Avant", "Explorer", "Méthode"),
		   array("8", "Avant", "Explorer", "Exemple"),
		   array("9", "Avant", "Exploser", "Clés"),
		   array("10", "Avant", "Exploser", "Méthode"),
		   array("11", "Avant", "Exploser", "Exemple"),
		   array("12", "Avant", "Examiner", "Clés"),
		   array("13", "Avant", "Examiner", "Méthode"),
		   array("14", "Avant", "Examiner", "Exemple"),
		   array("15", "Pendant", "", ""),
		   array("16", "Pendant", "Expliciter", "Clés"),
		   array("17", "Pendant", "Expliciter", "Méthode"),
		   array("18", "Pendant", "Expliciter", "Exemple"),
		   array("19", "Pendant", "Exécuter", "Clés"),
		   array("20", "Pendant", "Exécuter", "Méthode"),
		   array("21", "Pendant", "Exécuter", "Exemple"),
		   array("22", "Pendant", "Extrapoler", "Clés"),
		   array("23", "Pendant", "Extrapoler", "Méthode"),
		   array("24", "Pendant", "Extrapoler", "Exemple"),
		   array("25", "Après", "", ""),
		   array("26", "Après", "Exporter", "Clés"),
		   array("27", "Après", "Exporter", "Méthode"),
		   array("28", "Après", "Exporter", "Exemple"),
		   array("29", "Après", "Exposer", "Clés"),
		   array("30", "Après", "Exposer", "Méthode"),
		   array("31", "Après", "Exposer", "Exemple"),
		   array("32", "Après", "Exulter", "Clés"),
		   array("32", "Après", "Exulter", "Méthode"),
		   array("32", "Après", "Exulter", "Exemple"),
		   array("", "", "", ""));
    $items = array("Avant" => array("Explorer" , "Exploser", "Examiner"),
		   "Pendant" => array("Expliciter", "Exécuter", "Extrapoler"),
		   "Après" => array("Exporter","Exposer", "Exulter"));
    // Detects current $index and $page
    $split['index'] = $index = preg_replace('/.\/html\/img([0-9]*).html/', '$1', $source);
    foreach($pages as $page) if($index == $page[0]) break;
    // Builds the menu items
    $split['menu-when'] = ''; $split['menu-what'] = ''; $split['menu-how'] = '';
    // Builds menu-when
    foreach($items as $when => $what) {
      foreach($pages as $page0) if ($page0[1] == $when  && $page0[2] == '')  break;
      $split['menu-when'] .= ' '.($page[1] == $when && $page[2] == '' ? $when : '<a '.($page[1] == $when ? ' class="on"' : '').'href="page'.$page0[0].'.html">'.$when.'</a>');
    }
    // Adds default value to $page without wwhat and how
    $page1 = $page;
    {
      if ($page[1] == '') $page1[1] = $page[1] = "Avant"; 
      if ($page[2] == '') $page1[2] = $items[$page1[1]][0];
      if ($page[3] == '') $page1[3] = "Clés";
    }
    // Builds menu-what
    foreach($items[$page[1]] as $what) {
      foreach($pages as $page0) if ($page0[1] == $page1[1] && $page0[2] == $what && $page0[3] == $page1[3])  break;
      $split['menu-what'] .= ' '.($page[2] == $what ? $what : '<a href="page'.$page0[0].'.html">'.$what.'</a>');
    }
    // Builds menu-how
    foreach(array("Clés", "Méthode", "Exemple") as $how) {
      foreach($pages as $page0) if ($page0[1] == $page1[1] && $page0[2] == $page1[2] && $page0[3] == $how) break;
      $split['menu-how'] .= ' '.($page[3] == $how || $page[2] == 'Exulter' ? $how : '<a href="page'.$page0[0].'.html">'.$how.'</a>');
    }
    // Builds the related html
    {
      // Max number of slide
      $maxpage = 33;
      // Builds the HTML parts
      $split['header'] = '<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"><title>Projeter son projet</title><link rel="stylesheet" type="text/css" href="style.css"></head><body>';
      $split['trailer'] = '<center><img src="img'.$index.'.png" alt="Contenu de la slice"></center></body></html>';
      $split['link-home'] = '<a title="Home" href="index.html"><img style="height:40px" src="banner-mooc.png"/></a> <a title="Première page" href="page0.html"><img src="first.png"></a> '; 
      $split['link-prev'] = $index == 0 ? '<img src="left-inactive.png">' : '<a title="Page précédente" href="page'.($index-1).'.html"><img src="left.png"></a>';
      $split['link-next'] = $index == $maxpage - 1 ? '<img src="right-inactive.png">' : '<a title="Page suivante" href="page'.($index+1).'.html"><img src="right.png"></a>';
      $split['link-text'] ='<a style="float:right;margin-right:10px;" target="_blank" title="Version textuelle" href="text'.$index.'.html"><img src="text.png"></a>';
      $split['link-list'] ='<a style="float:right;margin-right:5px;"  <a title="Toutes les pages" href="toc.html"><img src="folder.png"></a>';
      // 
      $split['html'] = 
	$split['header'].
	'<div class="menu">'.
	$split['link-home'].
	'<div class="when">'.$split['menu-when'].'</div>'.
	$split['link-prev'].
	'<div class="what">'.$split['menu-what'].'</div>'.
	'<img src="more.png"/>'.
	'<div class="how">'.$split['menu-how'].'</div>'.
	$split['link-next'].
	$split['link-list'].
	$split['link-text'].
	'</div>'.
	$split['trailer'];
    }
    //- print_r($split);
    return $split;
  }
  // Applies to the ./html files
  foreach(scandir('./html') as $file) {
    if (preg_match('/^projeter.*\.html$/', $file)) {
      file_pregs_replace(array('/<title>Slide 1<\/title>/',
			       '/<h2>.*<\/h2>/',
			       '/Table of contents/', 
			       '/img([0-9]*)\.html/'),
			 array('<title>Projeter son projet</title>',
			       '<a title="La présentation" style="display:block; background-color:#f0f0f0; width:1200px; height:40px;" href="page0.html"><img style="float:left; height:40px;" src="banner-mooc.png"/></a>',
			       'Contenu',
			       'page$1.html'),
			 './html/'.$file, './html/toc.html');
    } else if (preg_match('/^img[0-9]*\.html$/', $file)) {
      $split = get_page_from_source('./html/'.$file);
      file_put_contents('./html/page'.$split['index'].'.html', $split['html']);
    }
  }
  file_put_contents('./html/index.html', '<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"><title>Projeter son projet</title><link rel="stylesheet" type="text/css" href="style.css"></head><body>
<a title="La présentation" style="display:block; background-color:#f0f0f0; width:1200px; height:40px;" href="page0.html"><img style="float:left; height:40px;" src="banner-mooc.png"/></a>
<table style="display:block; width:1200px; margin: 40px;"><tr>
 <td style="width:50%;"><iframe width="560" height="315" src="https://www.youtube.com/embed/E2bTJLs4bSY" frameborder="0" allowfullscreen></iframe></td>
 <td style="width:50%;"><a style="display:block;margin:auto;" title="La présentation textuelle" href="page0.html"><img style="display:block;margin:auto;" src="thumb0.png" width="400"></a></td></tr><tr>
<th>Vidéo de présentation de la méthode</th>
<th>Présentation textuelle (version <a class="link" href="projeter son projet.odp.pdf">PDF</a> et <a class="link" href="projeter son projet.odp.pptx">Éditable</a>)</th>
</tr></table>
</body></html>');
  file_put_contents('./html/style.css', '
.menu { display:block; background-color:#f0f0f0; width:1200px; height:40px; margin:0 auto; padding:0; }
.when, .what, .how { display:inline-block; vertical-align:top; font-size:20px; font-weight:bold; font-style:normal; color:grey; line-height:40px; margin:0; padding:0 5px;}
.when { background-color:#fffff0; }
.what { background-color:#fff0ff; }
.how  { background-color:#f0ffff; }
a { display:inline-block; color:black; text-decoration:none; }
a.on { color:grey; }
.menu img, .menu a img { height:40px; }
a.link { color:black; font-style:italic; }
a:hover.link { color:grey; }
');
}

?>
