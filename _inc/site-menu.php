<div id="menu-bar">
  <a title="Menu" href="javascript:elementToogle('menu', 'initial')" id="menu-bar-button"></a>
  <nav id="menu">
    <ul>
      <li><?php the_theme_context_link("page_id=2775", "Accueil");?></li>
      <li><?php the_theme_context_link("page_id=2697", "Actualités");?></li>
      <li><?php the_theme_context_link("page_id=36", "Ressources");?></li>
      <li><?php the_theme_context_link("page_id=40", "Interventions");?></li>
      <li><?php the_theme_context_link("page_id=9822", "Formations");?></li>
      <li><?php the_theme_context_link("page_id=42", "Contacts");?></li>
    </ul>
  </nav>
  <nav id="flux">
    <a title="RSS" href="<?php echo get_site_url(); ?>?cat=611&feed=rss2" id="menu-rss"></a>
    <a title="Twitter" href="<?php echo get_site_url(); ?>?page_id=3122" id="menu-twitter"></a>
    <a title="Youtube" target="_blank" href="https://www.youtube.com/user/Scienceparticipative" id="menu-youtube"></a>
    <a title="Pearltree" target="_blank" href="http://www.pearltrees.com/pixees_fr" id="menu-pearltree"></a>
    <a title="Recherche" href="javascript:elementToogle('search-container', 'initial')" id="menu-search"></a>
  </nav>
  <div id="search-container"><?php 
   echo preg_replace("|</form>|", "<input type='hidden' name='orderby' value='relevance'/></form>", get_search_form(false)); 
   ?></div>
</div>
