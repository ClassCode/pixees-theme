<aside id="sidebar">

  <section> 	  
    <h1>Actualités</h1>
    <ul>
      <li><h3><?php the_theme_context_link("page_id=2697", "À la une"); ?></h3></li>
      <li><h3><?php the_theme_context_link("page_id=2890", "Archives"); ?></h3></li>
      <li><h3><?php the_theme_context_link("page_id=3122", "Sur twitter"); ?></h3></li>
    </ul>
  </section>
  
  <div id="line-sidebar"></div>
  
  <?php include(get_template_directory().'/_inc/sidebar-item-gazette.php'); ?>

</aside>
