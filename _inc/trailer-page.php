<?php  include(get_template_directory().'/_inc/trailer-link.php');  ?>

<div id="trailer">
  <div id="participez-ht"><span id="pixees-participez-haut"></span></div>
  <section id="participez">  
    <span id="pixees-participez-bas"></span>
    <h1>participez !</h1>
    <ul>
      <li><a target="_blank" href="<?php echo $http_question;?>" id="question">une question ?</a></li>
      <li><a target="_blank" href="<?php echo $http_comment;?>" id="commentaire">un commentaire ?</a></li>
      <li><a target="_blank" href="<?php echo $http_partage;?>" id="partage">un contenu à partager ?</a></li>
    </ul>
  </section>
</div>
