<?php // Récupère un tableau sur docs.google.com et en fait une présentation HTML standard

$pages = array('projets', 'oeuvres', 'sites', 'jeux');

$gsheets = array(
 'projets' => 'https://docs.google.com/spreadsheets/d/1gHoPE_wdFWEXH0ZN1c5DvvMZGKjXvmUKW-rnBbi754g/export?exportFormat=csv',
 'oeuvres' => 'https://docs.google.com/spreadsheets/d/1px-4nj-YA4WVsKaFMjoOSbl89w_Xc38MVmpueLiI7UU/export?exportFormat=csv',
 'sites' => 'https://docs.google.com/spreadsheets/d/1egnNj9HAkfJhvqmlUG6aLKU02ZHg2XYD4g84GmSBfxo/export?exportFormat=csv',
 'jeux' => 'https://docs.google.com/spreadsheets/d/1hZg3CGAQvQvKYhgjMYTYL0PPqBUbKCP2sP23qgTBDJc/export?exportFormat=csv',
		 );
$headers = array(
		 'projets' => array('Timestamp', 'Titre', 'Description', 'Livrable possible', 'Questionnements liés au sujet', 'Séquençage putatif', 'Prérequis éventuels', 'Liens utiles', 'Email address', 'Votre nom ou pseudo'),
		 'oeuvres' => array('Timestamp', 'Titre', 'Description', 'Lien', 'Image (lien vers)', 'Questionnements possibles', 'Utilisation possible', 'Email address', 'Votre nom ou pseudo'),
		 'sites' => array('Timestamp', 'Adresse de mon site Web', 'Description éventuelle de mon travail', 'Qui je suis'),
		 'jeux' => array('Timestamp','Adresse Web du jeu vidéo', 'Description éventuelle de mon travail', 'Qui je suis'),						
		 );

// Gets a CSV file and properly manages \n in enclosure
function file_get_csv_contents($location, $delimiter = ',', $enclosure = '"', $escape = '\\') {
  $text = file_get_contents($location);
  for($in = false, $ii = 0; $ii < strlen($text); $ii++) {
    if (!$in && $text[$ii] == $enclosure) {
      $in = true;
    } else if ($in && $text[$ii] == $enclosure && ($ii > 0 && $text[$ii-1] != $escape)) {
      $in = false;
    }
    if ($in && $text[$ii] == "\n") $text[$ii] = "\v";
  }
  $data = array_map('str_getcsv', explode("\n", $text));
  foreach($data as &$row) foreach($row as &$cell) $cell = str_replace("\v", "\n", $cell);
  return $data;
}

// Récupération du tableau et création d'un tableau associatif
function gsheet2array($location, $what) {
  global $headers;
  $data = file_get_csv_contents($location);
  $header = array_shift($data);
  // Checks header 
  {
    foreach($headers[$what] as $name) if (!in_array($name, $header)) echo "<pre>Erreur: le terme $name n'est pas dans le tableau du gdrive, il y a du y avoir un changement.</pre>\n";
    foreach($header as $name) if (!in_array($name, $headers[$what])) echo "<pre>Erreur: le terme $name du tableau du gdrive n'est pas connu, il y a du y avoir un changement.</pre>\n";
  }
  $data = array_map(function($row) use ($header) { 
      if (count($header) != count($row))
	echo "<pre>Erreur: le header a ".count($header)." termes et la ligne ".count($row)."</pre>\n";
      foreach($row as $index => $value) {
	$row[$header[$index]] = $value; unset($row[$index]); 
      } 
      return $row; 
    }, $data);
  return $data;
}

// Génération du HTML {
function gsheet2html($location, $style = "", $what = '') {
  $data = array_reverse(gsheet2array($location, $what));
  $html = "<html>
  <head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'>
    <title>Éléments de partage du MOOC ICN</title>
    <style>$style</style>
  </head>
  <body>
  <table border='1px'>
";
  foreach($data as $item) {
    $html .= "<tr><td style='padding:10px;'>";
    switch($what) {
    case 'projets' : {
      $html .= "<h4>".$item['Titre']."</h4>".$item['Description'];
      foreach(explode("\n", $item['Liens utiles']) as $link) {
	$link = trim(preg_replace('/^[\s-]*/', '', $link));
	if ($link != '')
	  $html .= "(<a target='_blank' href='$link'>lien complémentaire</a>)";
      }
     $html .= ".<ul>";
     foreach(array('Livrable' => 'Livrable possible', 'Questionnements' => 'Questionnements liés au sujet', 'Séquençage' => 'Séquençage putatif', 'Prérequis' => 'Prérequis éventuels', 'Date de creation' => 'Timestamp') as $name => $key)
	if ($item[$key] != '')
	  $html .= "<li><i>$name</i> :".preg_replace("/\n/", "<br>", $item[$key])."</li>";
      $html .= "</ul>";
    } break;
    case 'oeuvres' : {
      $html .= "<h4><a target='_blank' href='".$item['Lien']."'>".$item['Titre']."</a></h4>";
      if ($item['Image (lien vers)'] != '')
	$html .= "<a target='_blank' style ='display:block;float:right' href='".$item['Lien']."'><img style='height:150px;' src='".$item['Image (lien vers)']."' alt='Illustration'/></a>";
      $html .= $item['Description']."<ul>";
      foreach(array('Questionnements' => 'Questionnements liés au sujet', 'Utilisation' => 'Utilisation possible', 'Date de creation' => 'Timestamp') as $name => $key)
	if ($item[$key] != '')
	  $html .= "<li><i>$name</i> :".preg_replace("/\n/", "<br>", $item[$key])."</li>";
      $html .= "</ul>";
    } break;
    case 'sites' : {
      $html .= "<h4><a target='_blank' href='".$item['Adresse de mon site Web']."'>*** lien vers le site ***</a></h4>";
      $html .= "<ul>";
      foreach(array('Autrice/teur' => 'Qui je suis', 'Description' => 'Description éventuelle de mon travail', 'Date de creation' => 'Timestamp') as $name => $key)
	if ($item[$key] != '')
	  $html .= "<li><i>$name</i> :".preg_replace("/\n/", "<br>", $item[$key])."</li>";
      $html .= "</ul>";
    } break;
    case 'jeux' : {
      $html .= "<h4><a target='_blank' href='".$item['Adresse Web du jeu vidéo']."'>*** lien vers le jeu ***</a></h4>";
      $html .= "<ul>";
      foreach(array('Autrice/teur' => 'Qui je suis', 'Description' => 'Description éventuelle de mon travail', 'Date de creation' => 'Timestamp') as $name => $key)
	if ($item[$key] != '')
	  $html .= "<li><i>$name</i> :".preg_replace("/\n/", "<br>", $item[$key])."</li>";
      $html .= "</ul>";
    } break;
    default:
      foreach($item as $name => $value)
 	$html .= "<div class='$name'><span class='what'>$name</span>$value</div>";
    }
    $html .=  "</td></tr>\n";
 }
 $html .="
    </table>
  </body>
</html>";
 return $html;
}

if (in_array($_REQUEST['what'], $pages))
  echo gsheet2html($gsheets[$_REQUEST['what']], '
.what { font-weight:bold; }
.what::after { content: " : "; }
', $_REQUEST['what']);
?>


