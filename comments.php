<?php
if (post_password_required())
  return;
?>

<div id="comments">
<?php 
  echo "<ul>";
  wp_list_comments(array('style'      => 'ul',
			 'short_ping' => true,
			 'avatar_size'=> 34));
  echo "</ul>";

  if (comments_open()) {
    // Ref: https://codex.wordpress.org/Function_Reference/comment_form#.24args
    $form_args = array( 
		       'title_reply' => 'Forum de discussion:');
    comment_form($form_args); 
  } else {
    echo "<p>La discussion est close.</p>";
  }
?>
</div>