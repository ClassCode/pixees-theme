<?php // Page displays functions

// Used for do_show_post_QRcode()
include(get_template_directory().'/show_post_QRcode/do_show_post_QRcode.php');
//global variable for V2 prepraration
$GLOBALS['classcode_v2_is_online'] = true;
$GLOBALS['classcode_profile_v2_is_online'] = false;

if(isset($_GET['v2']) && ($_GET['v2']=='false')){
  $GLOBALS['classcode_v2_is_online'] = false;
}

// Defines the contex of root categories
function root_category_to_context() {
  return array("presentation" => "presentation",
	       "formation" => "formation",
	       "definition" => "ressource",
	       "thematique" => "ressource",
	       "type-pedagogique" => "ressource",
	       "public-cible" => "ressource",
	       "intervention" => "intervention",
	       "a-la-une" => "actualite",
	       "gazette" => "actualite");
}
// Returns the context: accueil, actualite, ressource, presentation, formation, intervention, contact
function get_theme_context() {
  $post = get_post();

  // Patch over formation pages
  if (preg_match("|".get_site_url()."/classcode/formations/module[1-5-].*|", get_permalink()))
    return "classcode2";

  // classCode Page ? == parent of the "ClassCode" page
  $classCodePage = false;
  $ancestors = get_post_ancestors($post);
  if ($ancestors) {
    $ancestors = array_reverse($ancestors);
    foreach ($ancestors as $crumb) {
      if(get_the_title($crumb)=="ClassCode") {
	$classCodePage=true;
      }
    }
  }
  //classCode Post == category "Réunions ClassCode"
  if(in_category( 'Réunions ClassCode' )){
    $classCodePage=true;
  }
   
  if ($post->post_type == 'page' && !$classCodePage) {
    $post_root = $post; while($post_root->post_parent != 0) $post_root = get_post($post_root->post_parent);
    static $root_context = array(2775 => "accueil",
			  2697 => "actualite",
 			  9822 => "formation",
			  36 => "ressource",
			  40 => "intervention",
			  42 => "contact",
			  7993 =>"classcode",
			  11981 =>"classcode2");
    return isset($root_context[$post_root->ID]) ? $root_context[$post_root->ID] : "accueil";
  } else {
    $parentID = $post->post_parent; 
    $parent = get_post($parentID);
    $parentTitle = $parent->post_title;
    
    //if ($classCodePage || $post->post_type == 'include_post_type' || $post->post_type == 'quizz' || $post->post_type == 'rencontre')
    if ($classCodePage || $post->post_type == 'include_post_type' || $post->post_type == 'quizz')
      return "classcode";
    $cats = array_map(function($cat) { return $cat->slug; }, get_all_categories());
    foreach(root_category_to_context() as $category => $context)
      if (in_array($category, $cats))
	      return $context;
    return "accueil";
  }
}
// Returns all category ancestors of a post
function get_all_categories() {
  $cats = get_the_category();
  foreach($cats as $cat)
    while ($cat->parent != 0) {
      $cat = get_category($cat->parent);
      $cats[] = $cat;
    }
  return $cats;
}

function is_classcode2 ( $post ) {
  if (is_post_type_archive( "post" )) return false;
  $isPageClassCode2 = isset($_REQUEST['context']) && $_REQUEST['context'] == 'classcode2';
  $isPostClassCode2 = $post && get_post_meta( $post->ID, "post_a_la_carte", true );
  return $isPageClassCode2 || $isPostClassCode2;
}

function is_post_a_la_carte( $post ) {
	$term_a_la_carte = get_term_by( 'slug', "a-la-carte", "category" );
	$term_a_la_carte_id = $term_a_la_carte->term_id;
	$term_children = get_term_children ($term_a_la_carte_id , "category");
	array_push( $term_children, $term_a_la_carte_id);
	return has_category( $term_children, $post );
}

// Returns the post depth
function get_post_depth() {
  $depth = 0;
  $post = get_post();
  for($parent_id  = $post->post_parent; $parent_id > 0; $parent_id = $page->post_parent, $depth++)
    $page = get_page($parent_id);
  return $depth;
}
// Echos a link or a simple title depending on the current page
function the_theme_context_link($link, $title, $classcode=false) {
  if (preg_match("/(page_id|p|cat)=.*/", $link)) {
    $here = get_page_link();
    $hare = get_page_link(preg_replace("/(page_id|p|cat)=/", "", $link));
  } else {
    $here = preg_replace("/&alphabeticpage=./", "", $_SERVER["QUERY_STRING"]);
    $hare = preg_replace("/&alphabeticpage=./", "", $link);
  }
  //echo "<pre> #".preg_match("/(page_id|p|cat)=.*/", $link)." lnk=".preg_replace("/(page_id|p|cat)=/", "", $link)." here=$here hare=$hare </pre>";
  if($classcode){
    echo $here == $hare ? "<span class='active'>$title</span>" : "<a href=\"".get_site_url()."?$link\">$title</a>";  
  }else{
    echo $here == $hare ? "<i>$title</i>" : "<a href=\"".get_site_url()."?$link\">$title</a>";  
  }  
}


// Returns the 1st image in the content
function get_post_1st_img() {
  $content = get_the_content();
  if (($pos = strpos($content, "<img")) !== FALSE) {
    $content = preg_replace("/(<img[^>]*>).*/is", "\\1", substr($content, $pos));
    $content = preg_replace("/.*src\s*=\s*[\"']([^\"']*)[\"'].*/is", "\\1", $content);
    return $content;
  } else if(($pos = strpos($content, "https://player.vimeo.com/video/")) !== FALSE) {
    $img = get_post_meta(get_post()->ID, 'get_vimeo_image', true);
    if ($img == "") {
      $content = preg_split('#\r?\n#', ltrim($content), 0)[0];
      $videoId = preg_replace("|.*player.vimeo.com/video/([0-9]+).*|", "$1", $content);      
      $img = function_exists('get_vimeo_image') ? get_vimeo_image($videoId) : "img:get_vimeo_image($videoId)";
      update_post_meta(get_post()->ID, 'get_vimeo_image', $img);
    }
    return $img;
  } else 
    return false;
}

// Echoes the 1st image in the content as thumbnail
function the_theme_post_thumbnail($size = "height:100px;max-width:200px;margin-right:10px;") {
  $img = get_post_1st_img();
  if ($img) {
    echo "<img align=\"left\" style=\"".$size."\" src=\"".$img."\"/>";
    return true;
  } else
    return false;
}

// Returns the 1st link of the content 
function get_post_1st_link() {
  $content = get_the_content();
  if (preg_match("/^\s*https?:.*/", $content)) {
    $content = preg_replace("/\s*(http[^\s]*\s).*/is", "\\1", substr($content, strpos($content, "http"))); 
    return $content;
  } else if (($pos = strpos($content, "<a ")) !== FALSE) {
    $content = preg_replace("/(<a[^>]*>).*/is", "\\1", substr($content, $pos));
    $content = preg_replace("/.*href\s*=\s*[\"']([^\"']*)[\"'].*/is", "\\1", $content);
    return $content;
  } else
    return false;
}

// Echoes the 1st link of the content 
function the_theme_post_link($context) {
  $link = get_post_1st_link();
  if ($link) {
    echo "<div id=\"interline\"></div><div id=\"line-$context\"><a target='_blank' href='".$link."'>En savoir plus >>></a></div>";
    return true;
  } else
    return false;
}

// Echoes the sociable share line
function the_social_share($context) {
  if ($context != "presentation" && $context != "classcode") {
    if (the_modified_date("U", "", "", false) < 1409522400) {
      echo "<small>Dernière modification : avant août 2014</small>";
    } else {
      the_modified_date("F Y", "<small>Dernière modification : ", ".</small>");
    }
  }
  if (get_post_status(get_post()->post_ID) == 'obsolete') {
    echo " <small><b>Ce contenu est obsolète.</b></small>";
  }
  echo "<span id=\"social-icon-line\">";
  //if (function_exists("do_sociable")) { do_sociable(); } 
  echo preg_replace('/style="background-color:#000;"/', "", do_shortcode('[Sassy_Social_Share style="background-color:#000;"]'));
  if (function_exists("wpptopdfenh_display_icon")) { echo wpptopdfenh_display_icon(); }
  if (function_exists("do_show_post_QRcode")) { do_show_post_QRcode(); }
  if (function_exists('pf_show_link')) { echo pf_show_link(); }
  echo "</span>";  
  echo "<hr/><div>Vous pourriez aussi être intéressé-e-s par :<div id='mediego'>…/…</div></div>";
}

// Echoes a post in pixees format
function the_theme_post($is_single = false, $is_post = true, $has_trailer = true) {
  echo "<section id=\"contenant\">"; 
  $context = $_REQUEST['context']; // get_theme_context(); remis par vthierry le 14 janvier
  if ($is_post) {
    echo "<article id=\"post-".get_post()->post_ID."\" class=\"$context\">";
    if ($context != "presentation" && $context != "classcode") {
      if ($context != "accueil")
	echo "<h5>".ucfirst($context)."</h5>&nbsp;&nbsp;";
      echo "<span class=\"tag\">"; echo get_the_category_list("<small> . </small>", "", get_post()->post_ID); 
      echo "<i>"; the_tags("<small> . </small>", "<small> . </small>", ""); echo "</i></span>";
      echo "<div id=\"interline1\"></div>\n";
    }
  } else {
    echo "<div class=\"$context\">";
  }
  if ($is_single) {
    if($context != "classcode") {
      the_title("<h1>", "</h1>");
    }
    the_content();
    if ($has_trailer) {
      if ($is_post) {
	      if ($context != "presentation" && $context != "classcode"){
	        the_theme_post_link($context);
	        the_social_share($context);
        }
      }
    }
  } else {
    the_title("<h1><a href=\"".get_permalink()."\">", "</a></h1>");
    the_theme_post_thumbnail();
    echo preg_replace("/\[&hellip;\]$/","", get_the_excerpt()).' [<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><b>&hellip;</b></a>]';
  }  
  if ($is_post) {
    echo "</article>";
  } else {
    echo "</div>";
  }    
  if (comments_open()) {
    comments_template();
  }
  echo "</section>";
  if ($is_single && $has_trailer && $context != "classcode") {
    include(get_template_directory().'/_inc/trailer-'.($is_post ? "post" : "page").'.php');
  }
}

// Echoes all post/page in archive.php, page.php, search.php, single.php 
function the_theme_posts($is_single = false, $is_post = true, $with_alacarte = true, $has_trailer = true) {
  if (isset($_REQUEST['alphabeticpage'])) { 
    the_theme_alphabetic_posts();
  } else {
    if (have_posts()) {
      $next = false;
      while(have_posts()) {
	if ($next) {
	  echo "<div id=\"interline2\"></div><div id=\"line\"></div><div id=\"interline2\"></div>";
	} else {
	  $next = true;
	}
	the_post(); 
	if ($with_alacarte || !in_array('a-la-carte', get_all_categories()))
	  the_theme_post($is_single, $is_post, $has_trailer); 
      }
    } else
      include(get_template_directory().'/_inc/site-404.php');
  }
}

// Echoes all post of given alphabetic section
function the_theme_alphabetic_posts() {
  $letter = strtolower($_REQUEST['alphabeticpage']);
  $_REQUEST['context'] = "definition";
  include(get_template_directory().'/_inc/lexique-header.php');
  if (have_posts()) {
    $titles = array();
    $contents = array();
    while(have_posts()) {
      the_post();
      if (strtolower(substr(get_the_title(), 0, 1)) == $letter) {
	$titles[get_the_title()] = "<h4><a href=\"#post-".get_post()->ID."\">".get_the_title()."</a></h4>\n";
	{
	  ob_start();
	  echo "<a name=\"post-".get_post()->ID."\"></a><div id=\"interline\"></div><div id=\"line\"></div><div id=\"interline2\"></div>";
	  the_theme_post(true, true, false);
	  $contents[get_the_title()] = ob_get_contents();
	  ob_end_clean();
	}
      }
    }
    ksort($titles);
    ksort($contents);
    if (count($titles) > 0) {
      echo "<section id=\"glossaire\"><h1>".strtoupper($_REQUEST['alphabeticpage'])."</h1>";
      foreach($titles as $name => $title)
	echo $title;
      echo "</ul></section>";
      foreach($contents as $name => $content)
	echo $content;
    } else {
      echo "<div id=\"interline\"></div><h3 align=\"center\">Pas d'élément pour cette lettre</h3>";
    }
  } else
    include(get_template_directory().'/_inc/site-404.php');
  $_REQUEST['context'] = "ressource";
}

// Allows to insert a WP comment for  given page and given source and message attributes.
function sync_wp_comment($page_id, $fromLabel, $messageLabel) {
  if ($_REQUEST['page_id'] == $page_id && isset($_REQUEST[$fromLabel]) && isset($_REQUEST[$messageLabel]) && 
      (strncmp($_SERVER['HTTP_REFERER'], get_site_url(), strlen(get_site_url())) == 0)) {
  $comment_id = wp_new_comment(array('comment_post_ID' => $_REQUEST['page_id'],
				     'comment_author' => $_REQUEST[$fromLabel], 
				     'comment_author_email' => $_REQUEST[$fromLabel],
				     'comment_author_url' => "mailto:".$_REQUEST[$fromLabel], 
				     'comment_content' => $_REQUEST[$messageLabel],
				     'comment_type' => 'comment',
				     'comment_parent' => 0,
				     'user_id' => 0,
				     //'comment_author_IP',
				     ));
  }
}
// Allows to insert a WP comment for the theme comment pages
function sync_wp_comments() {
  // Interfaces with the datagramme comment page
  if (isset($_REQUEST['page_id']) && $_REQUEST['page_id'] == 4278 && isset($_REQUEST['message']) && isset($_REQUEST['email'])) {
    $souhait = array(
		     'commenter la réponse à cette question/réponse',
		     'proposer une correction à cette question/réponse',
		     'proposer une nouvelle question/réponse',
		     'signaler un bug dans le jeu datagramme',
		     'faire une suggestion pour le jeu datagramme');
    $_REQUEST['fromLabel'] = 
      $_REQUEST['full_name'].'<'.$_REQUEST['email'].'>';
    $_REQUEST['messageLabel'] = 
      '<strong>'.$_REQUEST['Question-datagramme'].'</strong>'.
      (strlen($_REQUEST['Numero-de-la-question']) > 0 ? ' (#'.$_REQUEST['Numero-de-la-question'].') <br/>' : '').
      '<em> Je souhaite '.$souhait[$_REQUEST['Vous-souhaitez'][0]-1].'.</em><br/>'.
      $_REQUEST['message'];
    // echo '<hr><pre style="background-color:yellow;">'; print_r($_REQUEST); echo '</pre><hr>'; 
    sync_wp_comment(4278, 'fromLabel', 'messageLabel');
  }
}
// Interfaces with Fast Secure Contact Form 
function fill_fscf_fields() {
  echo '<script src="'.get_site_url().'/wp-content/themes/pixees-theme/_inc/trailer-form-wrapper.js" type="text/javascript"></script>';
  echo '<script language="javascript">'; foreach($_REQUEST as $name => $value) if (preg_match("/^fscf_.*$/", $name)) echo "getQueryVar('$name');"; echo '</script>';
  echo '<script language="javascript"> var names = document.getElementsByName("email"); for(var i in names) names[i].value = "'.wp_get_current_user()->user_email.'"; </script>';
}

// Implements a short-code implementing a pop-down section
function popdown_shortcode($atts, $content = "") {
  //$content = do_shortcode($content);
  if (isset($atts['title']) && $content != "") {
    $id = sanitize_html_class($atts['title']);
    return '<div id="popdown_'.$id.'" style="width:100%;clear:both;" class="popdown"><h4 class="popdown-inline" style="width:100%;padding:0px;"><a style="display:block;padding:0px 0px 0px 40px;background:url(\''.get_template_directory_uri().'/_img/menu-popdown-1.png\') 0 0px no-repeat;height:100%;width:auto;" href="#" onClick="var div = document.getElementById(\'popdown_'.$id.'_div\'); if (div.style.display == \'none\') { div.style.display = \'block\'; setCookie(\'popdown_'.$id.'_display\', \'block\'); this.style.background = \'url(\\\''.get_template_directory_uri().'/_img/menu-popdown-2.png\\\') no-repeat\'; } else { div.style.display = \'none\'; setCookie(\'popdown_'.$id.'_display\', \'none\'); this.style.background = \'url(\\\''.get_template_directory_uri().'/_img/menu-popdown-1.png\\\') no-repeat\'; } return false;" id="popdown_'.$id.'_a">'.$atts['title'].'</a></h4><div class="popdown-inline-body" style="display:none;clear:both;margin-top:-25px;" id="popdown_'.$id.'_div">'.html_entity_decode($content).'</div></div><script>if (getCookie(\'popdown_'.$id.'_display\') != \'\') document.getElementById(\'popdown_'.$id.'_div\').style.display = getCookie(\'popdown_'.$id.'_display\');</script>';
  } else if (isset($atts['post_id'])) {
    $post = get_post($atts['post_id']);
    if ($post) {
      if (isset($atts['open'])) {
	return '<div id="popdown_'.$atts['post_id'].'" style="width:100%;clear:both;" class="popdown"><h3 class="popdown-include" style="padding:0px 0px 0px 10px;">&nbsp;&nbsp;'.$post->post_title.'</h3><div class="popdown-include-body" style="display:block;clear:both;margin-top:-18px;" id="popdown_'.$atts['post_id'].'_div">'.str_replace(']]>', ']]&gt;', apply_filters('the_content', $post->post_content)).'</div></div>';
      } else {
	return '<div id="popdown_'.$atts['post_id'].'" style="width:100%;clear:both;" class="popdown"><h3 class="popdown-include" style="padding:0px 0px 0px 10px;"><a style="display:inline-block;width:'.(isset($atts['link']) ? '95' : '100').'%;height:100%;" href="#" onClick="var div = document.getElementById(\'popdown_'.$atts['post_id'].'_div\'); if (div.style.display == \'none\') { div.style.display = \'block\'; setCookie(\'popdown_'.$atts['post_id'].'_display\', \'block\'); } else { div.style.display = \'none\'; setCookie(\'popdown_'.$atts['post_id'].'_display\', \'none\'); } return false;" id="popdown_'.$atts['post_id'].'_a">&nbsp;&nbsp;'.$post->post_title.'</a>'.
	  (isset($atts['link']) ? '<a style="display:inline-block;float:right;" href="'.get_site_url().'?p='.$atts['post_id'].'">> &nbsp;</a>' : '').
	  '</h3><div class="popdown-include-body" style="display:none;clear:both;margin-top:-18px;" id="popdown_'.$atts['post_id'].'_div">'.str_replace(']]>', ']]&gt;', apply_filters('the_content', $post->post_content)).'</div></div><script>if (getCookie(\'popdown_'.$atts['post_id'].'_display\') != \'\') document.getElementById(\'popdown_'.$atts['post_id'].'_div\').style.display = getCookie(\'popdown_'.$atts['post_id'].'_display\');</script>';
      }
    } else 
      return '<pre>[popdown error, [popdown post_id="'.$atts['post_id'].'"] with unknown post]</pre>';
  } else
    return '<pre>[popdown error, [popdown title="the title"]the body[/popdown] or [popdown post_id="$postid"] is the correct syntax]</pre>';
}
add_shortcode("popdown", "popdown_shortcode");
//add_shortcode("popdown2", "popdown_shortcode");

// Implements a short-code implementing the register button if not yet connected else profile modificaton
function cc_register_shortcode($atts = NULL, $content = "") {
  return is_user_logged_in () ? 
    '<div id="signin" class="contentDesc"><a href="'.bp_loggedin_user_domain().'profile/edit/group/1">mon profil</a></div>' : 
    '<div id="signin" class="contentDesc"><a href="'.get_site_url().'/wp-login.php?action=register">s\'inscrire</a></div>';
}
add_shortcode("cc_register", "cc_register_shortcode");

// Implements a short-code implementing the display profile functionality
function cc_profile_shortcode($atts = array(), $content = "") {
  include_once(WP_PLUGIN_DIR.'/class_code/bp-profiles.php');
  $userID = isset($atts['userID']) ? $atts['userID'] : wp_get_current_user()->ID;
  return is_user_logged_in () ? 
    '<div id="profilDisplay" class="contentDesc">'.display_classcode_profile($userID).'</div>' : 
    '<div id="contentTitleResume" class="contentDesc"><h2>Bienvenu(e)s</h2>
<p>Class´Code propose un programme de formation gratuit à destination de toutes personnes désireuses d’initier les jeunes de 8 à 14 ans à la <a target="_blank" href="https://project.inria.fr/classcode/mais-pourquoi-classcode-parle-de-pensee-informatique/">pensée informatique</a>. Le programme comprend de 1 à 5 modules de formation en ligne d’une dizaine d’heures chacun, couplé à des temps de rencontre présentielle pour partager, expérimenter et échanger entre apprenants.</p>
<p>Bienvenue sur Class´Code.</p>
</div>';
}
add_shortcode("cc_profile", "cc_profile_shortcode");

// Implements a short-code implementing the display mini profile functionality
// available colors are : classCodeMiniBlue classCodeMiniDarkBlue classCodeMiniGreen classCodeMiniPink
function cc_profile_mini_shortcode($atts, $content = "") {
  $user_id = $atts['user_id'];
  $user = get_userdata( $user_id );
  ob_start();  
  if($user === false ){
    
  }else{
    echo "<div class='classCodeMiniProfil ".$atts['color_class']."' data-user_id='".$user_id."'>";
      echo "<div class='classCodeMiniAvatar'>";
        class_code_user_avatar_img_display(bp_get_profile_field_data(array('field' => 'AvatarImg', 'user_id' => $user_id)));  
      echo "</div>";
      echo "<div class='classCodeMiniName'>";
       echo mb_strimwidth(ucwords(strtolower(getDisplayName($user_id))), 0, 15, "...");
      echo "</div>";
    echo "</div>";
  }
  return ob_get_clean();
}
add_shortcode("cc_profile_mini", "cc_profile_mini_shortcode");

// Implements a short-code implementing the banner section
//  - either consider the popdown ids
//  - or manually sets a menu with syntax id:title,id:title,...
function cc_banner_shortcode($atts, $content = "") {
  $post = get_post();
  $title = isset($atts['title']) ? $atts['title'] : $post->post_title;
  $html = '
<div id="contentTitle">';
  {
    $html .= '<div id="docHeader" class="contentMedia">'.($title == "" ? '' : '<h1>'.$title.'</h1>');
    if (isset($atts['menu'])) {
      $items = array_map(function($item) { return explode(":", $item); }, explode(',', $atts['menu']));
      $nn = 1;
      foreach($items as $item)
	if (count($items) > 1)
	  $html .= '<a href="#'.$item[0].'"><span class="docHeaderItem '.$item[0].'"><table><td><span class="bigNumber">'.($nn++).'</span></td><td>'.$item[1].'</span></td></table></a>';
	else
	  $html .= '<a href="#'.$item[0].'"><span class="docHeaderItem '.$item[0].'">'.$item[1].'</span></a>';
    } else if (isset($atts['popdown']) && $atts['popdown'])
      foreach(preg_grep_split('/popdown *post_id=["\']?([0-9]*)["\']?/', $post->post_content) as $id)
	$html .= '<a href="#popdown_'.$id.'" onClick="document.getElementById(\'popdown_'.$id.'_div\').style.display = \'block\'; setCookie(\'popdown_'.$id.'_display\', \'block\');"><div class="docHeaderItem">'.get_post($id)->post_title.'</div></a>';
    else
      $html .= $content;
    $html .= '</div>';
  }
  return $html.cc_profile_shortcode().'
<div id="contentTitleFooter"><div id="contentTitleImageFooter" class="contentMedia"></div>'.
    cc_register_shortcode().'
</div></div>';
}
// Extract all pattern (16 max) and replace by the captured substring
function preg_grep_split($pattern, $string) {
  $items = array();
  for($offset = 0, $nn = 0; $nn < 10 && preg_match($pattern, $string, $match, PREG_OFFSET_CAPTURE, $offset) == 1; $nn++, $offset = $match[0][1] + 1) {
    $items[] = preg_replace($pattern, "$1", $match[0][0]);
  }
  return $items; 
}
add_shortcode("cc_banner", "cc_banner_shortcode");

// Implements the shortcode to represent the MOCC in the frontend page
// @param atts The shortcode parameters:
// - title   The module title.
// - img     The module image.
// - url     The module url on the image, if defined.
// - opening The module opening date in the future; if defined, adds a opening banner.
// - module  The module number; if defined, defined creates links towards the ressource and meeting page.
// @param content The shortcode HTML text.
function cc_moocblock_shortcode($atts, $content = "") {
  include_once(WP_PLUGIN_DIR.'/class_code/ClassCode_config.php');
  global $title_module;
  global $cc_moocblock_shortcode_id;
  $cc_moocblock_shortcode_id++;
  $html = '<div id="mooc_'.$cc_moocblock_shortcode_id.'" class="moocsBloc">';
  $html.= '<h3>'.$atts['title'].'</h3>';
  $html.= '<div class="moocsMedia">'.(isset($atts['url']) ? '<a href="'.$atts['url'].'" target="_blank"><img src="'.$atts['img'].'" alt="Icone du module" /></a>' : '<img src="'.$atts['img'].'" alt="Icone du module" />').'</div>';
  $html.= '<div class="moocsDesc"><div class="moocsDesc_content">'.$content.'</div>';
  if(isset($atts['module'])){
    if(isset($title_module[$atts['module']])){
      $html.= "<a href='/classcode/formations/v1-module".$atts['module']."' class='moocsDesc_grey_module_box openClassroomLink'>Démarrez la formation en ligne</a>";
      $html.= "<a href='/classcode/formations/v1-module".$atts['module']."#meeting' class='moocsDesc_grey_module_box meetingListLink'>Trouver un temps de rencontre près de chez vous</a>";
    }
  } 
  $html .= '</div>';
  if (isset($atts['opening']))
    $html.= '<div class="moocsOpening"><span class="moocsOpeningText">Ouverture | '.$atts['opening'].'</span></div>';
  $html .= '</div>';
  return $html;
}
$cc_moocblock_shortcode_id = 0;
add_shortcode("cc_moocblock", "cc_moocblock_shortcode");

// Inserts an tooltip shortcode
// - picto : either gray or white
// - url: the help link if any
// The help description is in the texte
function cc_tooltip_shortcode($atts, $content = "") {
  ob_start();
  echo_help_tooltip($content, isset($atts['picto']) ? $atts['picto'] : "white", $atts['url']);
  return ob_get_clean();
}
add_shortcode("cc_tooltip", "cc_tooltip_shortcode");

// Implements module shortcode
function cc_module_video_shortcode($atts, $content = "") {
  return '<p class="module-video"><a class="module-video" title="'.$atts['title'].'" href="'.$atts['url'].'" rel="shadowbox" onclick="Shadowbox.open({content: this.href, player: \'iframe\', title: this.title, width:window.innerWidth, height:(window.innerHeight-120)}); return false;" >'.($atts['notext'] ? "" : $atts['title']).'</a></p>';
}
add_shortcode("cc_module_video", "cc_module_video_shortcode");

// Implements the echo of a textual field with automatic completion
// @param $name The field ID and name
// @param $values An array with the predefined values, or a semicolumn or comma or space separated string with the values.
// @param $default A predefined value, if any
// @param $class The display class, if any
// @param $predefined If true define a select field withe predefined values
// @param $placeholder The string displayed before a value input
// @return The HTML implementation of the field
function do_completion_field($name, $values, $default = "", $class = '', $predefined = false, $placeholder='', $title='') {
  $id = str_replace('-', '_', sanitize_html_class($name));
  $values = is_array($values) ? $values : explode(strpos($values, ";") !== FALSE ? ";" : strpos($values, ",") !== FALSE ? "," : " ", $values);
  if ($predefined) {
    $input = '<div class="ui-widget"><select class="'.$class.'" name="'.$name.'" id="'.$name.'">';
    if($placeholder!=''){
      $input .= "<option value='' default selected style='font-style: italic;'>".$placeholder." :</option>";
    }
    $input .= implode(" ", array_map(function($value) { return '<option value="'.$value.'" '.($value == $default ? 'selected="selected"' : '').'>'.$value.'</option>'; }, $values)).'</select></div>';
    
    return $input;
  } else {
    return '<div class="ui-widget"><input placeholder="'.$placeholder.'" title="'.$title.'" class="'.$class.'" id="'.$name.'" name="'.$name.'" value="'.$default.'" ></div><script>var '.$id.' = '.json_encode($values, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK).'; jQuery(document).on("keydown", "#'.$name.'", function() { jQuery("#'.$name.'").autocomplete({ source: '.$id.'})});</script>';
  }
}
add_action('wp_enqueue_scripts', function() {
    wp_enqueue_script('jquery');
    //wp_enqueue_script('jquery-ui-autocomplete');
    wp_enqueue_script('cc-jquery-ui', get_template_directory_uri()."/autocomplete/jquery-ui.min.js");
    wp_enqueue_style('jquery-ui-autocomplete', get_template_directory_uri()."/autocomplete/jquery-ui.css");
  });
function completion_field_shortcode($atts, $content = "") {
  return do_completion_field($atts['name'], $atts['values'], $atts['value'], $atts['style'], $atts['predefined']);
}
add_shortcode("completion_field", "completion_field_shortcode");

// Inserts a server mail form
function cc_mailer_shortcode($atts, $content = "") {
  $name = "cc_mailer_shortcode";
  $who = preg_replace('/"/', '&quot;', isset($_REQUEST[$name.'_who']) ? urldecode($_REQUEST[$name.'_who']) : (isset($atts['who']) ? $atts['who'] : ''));
  $to = preg_replace('/"/', '&quot;', isset($_REQUEST[$name.'_to']) ? urldecode($_REQUEST[$name.'_to']) : (isset($atts['to']) ? $atts['to'] :  ''));
  if (isset($_REQUEST[$name.'_subject']))
    $_REQUEST[$name.'_subject'] = preg_replace("/\\\\?'/", "´",  $_REQUEST[$name.'_subject']);
  if (isset($_REQUEST[$name.'_body']))
    $_REQUEST[$name.'_body'] = preg_replace("/\\\\?'/", "´",  $_REQUEST[$name.'_body']);
  $body = isset($_REQUEST[$name.'_body']) ? urldecode($_REQUEST[$name.'_body']) : '';
  if ((!isset($_REQUEST[$name.'_submit'])) && (isset($_REQUEST['body_completion']) || !isset($_REQUEST[$name.'_body'])))
    $body = "Bonjour,&#13;&#10;&#13;&#10;".$body.
      (wp_get_current_user()->ID != 0 ? "&#13;&#10;&#13;&#10;".(function_exists("bp_get_profile_field_data") ?
         bp_get_profile_field_data(array('field' => 'Prénom', 'user_id' => wp_get_current_user()->ID))." ".
         bp_get_profile_field_data(array('field' => 'Nom', 'user_id' => wp_get_current_user()->ID))." " : "").
       " ".get_site_url()."/members/".wp_get_current_user()->user_nicename : "");
  // Sends the mail if the verify_nonce is correct
  $sent = "<p style='float:left;color:#266d83;'>&nbsp;</p>";
  if (isset($_REQUEST[$name.'_to']) && isset($_REQUEST[$name.'_subject']) && isset($_REQUEST[$name.'_body'])
      && (wp_get_current_user()->ID != 0) 
      && isset($_POST[$name.'_data_nonce']) && wp_verify_nonce($_POST[$name.'_data_nonce'], $name.'_id_nonce')) {
    if (mail($_REQUEST[$name.'_to'],  iconv( "UTF-8", "ISO-8859-1//IGNORE",$_REQUEST[$name.'_subject']), $_REQUEST[$name.'_body'], 'From: '.wp_get_current_user()->user_email."\r\nContent-type: text/plain; charset=utf-8\r\nContent-Transfer-Encoding: 8bit\r\n"))
      $sent = "<p style='float:left;color:#266d83;margin-top:20px;font-size:26px;font-weight:bold;'>Le courriel a été envoyé.</p>";
    else
      $sent = "<p style='float:left;color:#266d83;margin-top:20px;font-size:26px;font-weight:bold;'>L'envoi du courriel a échoué.</p>";
  }
  $html = "";
  // Displays the form
  $html .= '<form id="'.$name.'_form" onSubmit="return confirm(\'Ok pour l´envoi du message ?\');" action="#'.$name.'_form" method="post">';
  $html .= wp_nonce_field($name.'_id_nonce', $name.'_data_nonce', true, false);
  $html .= '<table>
<tr><td valign="top"><label for="to">Destinataire:</label></td><td><input style="width:600px;" type="text" readonly id="'.$name.'_who" name="'.$name.'_who" value="'.$who.'"><input id="'.$name.'_to" name="'.$name.'_to" type="hidden" value="'.$to.'"/></td></tr>
<tr><td valign="top"><label for="subject">Sujet:</label></td><td><input placeholder="Ma question" style="width:600px;" id="'.$name.'_subject" name="'.$name.'_subject" type="text" value="'.preg_replace('/"/', '&quot;', isset($_REQUEST[$name.'_subject']) ? urldecode($_REQUEST[$name.'_subject']) : '').'"/></td></tr>
<tr><td valign="top"><label for="body">Message:</label></td><td><textarea style="width:600px;height:200px;" id="'.$name.'_body" name="'.$name.'_body">'.preg_replace('/</', '&lt;', $body).'</textarea></td></tr>'.(wp_get_current_user()->ID != 0 ?
  '<tr><td align="right" colspan="2">'.$sent.'<input class="sendmail" type="submit" name="'.$name.'_submit" value="Envoyer"/></td></tr>' :
  '<a style="display:block;float:right;padding:2px 10px;min-width:10px" class="sendmail" href="javascript:'.$name.'_on_submit();" class="button">Message</a>').'
</table></form>
<script language="javascript">
function '.$name.'_on_submit() {
  window.location.href = "mailto:" + encodeURI(document.getElementById("'.$name.'_to").value)+"?subject="+encodeURI(document.getElementById("'.$name.'_subject").value)+"&body="+encodeURI(document.getElementById("'.$name.'_body").value);
}
function '.$name.'_on_request(data) {
  document.getElementById("'.$name.'_to").value = decodeURI(data["to"]);
  document.getElementById("'.$name.'_who").value = decodeURI(data["who"]);
  document.getElementById("'.$name.'_subject").value = decodeURI(data["subject"]);
  document.getElementById("'.$name.'_body").value = decodeURI(data["body"]);
  window.location.href = "#aideAction2";
}
</script>'; 
  return $html;
}
add_shortcode("cc_mailer", "cc_mailer_shortcode");
// Inserts a link towards the cc_mailer shortcode
function cc_mailto_shortcode($atts, $content = "") {
  $name = "cc_mailer_shortcode";
  $link = get_site_url()."/classcode-v2/contacts";
  $url = preg_replace("/\/?\?.*/", "", (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
  //echo "<pre>".$link." ".$url."</pre>";
  $to_value = rawurlencode(isset($atts['to']) ? $atts['to'] : 'classcode-accueil@inria.fr');
  $who_value = rawurlencode(isset($atts['who']) ? $atts['who'] : 'Bureau d´Accueil de Class´Code');  
  $logo_value = rawurlencode(isset($atts['logo']) ? $atts['logo'] : get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png');
  $subject_value = isset($atts['subject']) ? rawurlencode($atts['subject']) : '';
  $body_value = isset($atts['body']) ? rawurlencode(preg_replace("/\\n/", "&#13;&#10;", $atts['body'])) : '';
  if(true /*$url != $link*/) { // Cannot use the javascript:*_on_request() construction because wordpress kills javascript as safety fucking rule
    return $link.'?'.$name.'_to='.$to_value.'&'.$name.'_who='.$who_value.'&'.$name.'_subject='.$subject_value.'&'.$name.'_logo='.$logo_value.'&'.$name.'_body='.$body_value.'&body_completion=true#aideAction2';
  } else {
    return 'javascript:'.$name.'_on_request({to:\''.$to_value.'\',who:\''.$who_value.'\',subject:\''.$subject_value.'\',body:\''.$body_value.'\'});';
  }
}
add_shortcode("cc_mailto", "cc_mailto_shortcode");

// Inserts an help requirement shortcode
// - picto : either gray or white
// - title: the help title in the popup
// - subject: the help subject for the help form
// - body: the help message body
function cc_help_shortcode($atts, $content = "") {
  $atts['subject'] = isset($atts['subject']) ? $atts['subject'] : "Aide à propos de Class´Code";
  $link = cc_mailto_shortcode($atts);
  return '
<span style="float:right" class="helptooltip" >
  <a target="_help" href="'.$link.'"><img class="helpImg2" alt="helpIcone" src="'.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_pictos/'.(isset($atts['picto']) && $atts['picto'] == 'gray' ?  'i-tooltip-aide-gray.png' : 'i-tooltip-aide.png').'" >
  <a target="_help" href="'.$link.'"><span class="helptooltiptext helptooltip-top">'.$atts['title'].'</span></a>
</span>';
}
add_shortcode("cc_help", "cc_help_shortcode");
// Inserts a coordinateur-e-s contact predefinition form
function cc_coordinator_contact_shortcode($atts, $content = "") {
  include_once(WP_PLUGIN_DIR.'/class_code/ClassCode_config.php');
  $var = '';
  $sel = '';
  if (isset($data_coordinations)) {
    foreach($data_coordinations as $name => $data)
      if (is_array($data)) {
	$var .= ($var == '' ? '' : ',').' { name:"'.$name.'", logo:"'.$data['logo'].'", url:"'.$data['url'].'", who:"'.$data['who'].'", email:"'.$data['email'].'" }';
	$sel .= '<option value="'.$name.'">'.$name.'</option>';
      }
    return '<div style="height:120px;">
<select onChange="cc_coordinator_contact_on_change(this.selectedIndex);"><option selected=\"selected\">Sélectionner une coordination</option>'.$sel.'"</select>
<a style="display:none;font-size:18px;font-weight:bold;line-height:33px;background-color: #266d83;color: #fff;cursor: pointer;padding:2px 10px;" id="cc_coordinator_contact_to" href="#">Contacter</a>
<a id="cc_coordinator_contact_link" style="display:block;float:right;" title="#" href="#"><img id="cc_coordinator_contact_img" width="180" src="#" alt="Class´Code"/></a></div>
<script language="javascript">
  var cc_coordinator_contact_data = [ '.$var.' ]
  function cc_coordinator_contact_on_change(selectedIndex) {
    if((--selectedIndex) >= 0) {
      document.getElementById("cc_coordinator_contact_to").style.display = "inline-block";
      document.getElementById("cc_coordinator_contact_img").src = cc_coordinator_contact_data[selectedIndex]["logo"];
      document.getElementById("cc_coordinator_contact_link").href = cc_coordinator_contact_data[selectedIndex]["url"];
      document.getElementById("cc_coordinator_contact_link").title = "À l´attention de "+cc_coordinator_contact_data[selectedIndex]["who"]+", "+cc_coordinator_contact_data[selectedIndex]["name"];
      document.getElementById("cc_coordinator_contact_img").alt = cc_coordinator_contact_data[selectedIndex]["name"];
      document.getElementById("cc_coordinator_contact_to").href = "https://classcode.fr/accueil/aide/?cc_mailer_shortcode_to="+encodeURI(cc_coordinator_contact_data[selectedIndex]["email"])+"&cc_mailer_shortcode_who="+encodeURI(document.getElementById("cc_coordinator_contact_link").title)+"&cc_mailer_shortcode_subject="+encodeURI("À propos de Class´Code coordonné par "+cc_coordinator_contact_data[selectedIndex]["name"])+"#aideAction2";
    } else {
      document.getElementById("cc_coordinator_contact_to").style.display = "none";
      document.getElementById("cc_coordinator_contact_img").src = "'.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png";
      document.getElementById("cc_coordinator_contact_link").href = "https://classcode.fr";
      document.getElementById("cc_coordinator_contact_img").alt = "Class´Code";
      document.getElementById("cc_coordinator_contact_link").title = "À l´attention du Bureau d´accueil de Class´Code";
    }
  }
  cc_coordinator_contact_on_change(0);
</script>';
  } else 
    return '<pre>Upps: la structure de donnée des coordinateurs n´est pas définie</pre>';
}
add_shortcode("cc_coordinator_contact", "cc_coordinator_contact_shortcode");

//shortcode pour le formulaire de recherche (et la recherche) des rencontres
function cc_meeting_search_shortcode($atts, $content = "") {
  include_once(WP_PLUGIN_DIR.'/class_code/ClassCode_config.php');

  global $title_module;
  global $data_structures;
  global $wpdb;
  $visitorId = bp_loggedin_user_id();  
 
  $visitorLocation = $wpdb->get_row($wpdb->prepare( "SELECT * FROM wppl_friends_locator WHERE member_id = %s", $visitorId ) );
  $visitorLat = $visitorLocation->lat;
  $visitorLong = $visitorLocation->long;
  $noVisitorLocation =false;
  $mapZoom = '11';
  if($visitorLat =='' || $visitorLong == ''){
    $visitorLatLong =  '46.52863469527167,2.43896484375'; 
    $mapZoom = '5';
    $noVisitorLocation = true;
  }else{
    $visitorLatLong = strval($visitorLat).",".strval($visitorLong);
  }
  $visitorDisplayName = getDisplayName($visitorId);
  if(isset($_POST['search'])){
    $what=array();
    if(isset($_POST['mymeeting']) && $_POST['mymeeting'] ){
      $what['mes_rencontres']=true;       
    }
    if(isset($_POST['where'])){
      if($_POST['where']!='everywhere'){
        $what['nearby']=$_POST['where'];  
      }else{
        $what['where']=$_POST['where']; 
      }
    }else{
      $what['where']='everywhere'; 
    }
    if(isset($_POST['module']) && $_POST['module'] ){
      $what['module']=$_POST['module'];       
    }
    if(isset($_POST['structure']) && $_POST['structure'] ){
      $what['structure']=$_POST['structure'];       
    }
    if(isset($_POST['when']) && $_POST['when'] ){
      $what['when']=$_POST['when'];       
    }
    $rencontres = rencontre_post_type::get_rencontres($what);
  }else{
    //recherche par défaut
    if (is_user_logged_in()) {
      $what['where']='everywhere';
    }
    $what['when']='future';    
    // Ici ajoute les valeurs par defaut du shortcode
    if(isset($atts) && is_array($atts)){
      if(array_key_exists('module',$atts)){
        if(isset($atts['module'])){
          $atts['module']=$title_module[$atts['module']];  
        }        
      }
      $what = array_merge($what, $atts);  
    }
    if(isset($_GET['module']) && array_key_exists($_GET['module'],$title_module)){
      $what['module']=$title_module[$_GET['module']];  
    }
    
    //- echo "<pre>".print_r($what, true)."</pre>";
    $rencontres = rencontre_post_type::get_rencontres($what);
  }
  
  global $meetings;
  $meetings  = array();
  $meeting_inc = 0;
  //counters
  $allNbre = 0;
  $pastNbre = 0;
  $futureNbre = 0;  
  $modulesNbreArray = array();
  foreach($title_module as $title){
    $modulesNbreArray[$title]=0;
  }
  $modulesNbreOther = 0;
  $nearbyNbre = 0;

  foreach($rencontres as $rencontre) { 
    $post_id = $rencontre['post']->ID;
    $organizerId = $rencontre['post']->post_author;
    $postLocation = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM wp_places_locator WHERE post_id = %d", array( $post_id ) ) );                
    $meetings[$meeting_inc]['post_id']=$post_id;
    $meetings[$meeting_inc]['userStatut']="visiteur";
    if($visitorId == $organizerId){
      $meetings[$meeting_inc]['userStatut']="organisateur";
    }else{      
      $participants = get_post_meta($post_id, 'rencontre_participants', true);
      $participants_array = array_values(array_filter(explode('|',$participants )));
      $meetings[$meeting_inc]['nbParticipants']=count($participants_array);
      $participants_inc = 0;
      $userFound = false;
      while(!$userFound && ($participants_inc <$meetings[$meeting_inc]['nbParticipants'])){
        if($participants_array[$participants_inc] == $visitorId){
          $userFound = true;
        }
        $participants_inc++;
      }
      if($userFound){
        $meetings[$meeting_inc]['userStatut']="participant";
      }
    }
    $meetings[$meeting_inc]['structure']=get_post_meta($post_id, 'structure', true);
    if(!$meetings[$meeting_inc]['structure'] || $meetings[$meeting_inc]['structure'] == ''){
      $organizerdisplayName = getDisplayName($organizerId);
      $meetings[$meeting_inc]['structure']=$organizerdisplayName;
    }
    $meetings[$meeting_inc]['lieu']= str_replace("\\", "",$postLocation->formatted_address) ;
    if(!isset($postLocation->lat) || $postLocation->lat =='' || !isset($postLocation->long) || $postLocation->long == ''){
      $meetings[$meeting_inc]['latlong'] = '';
    }else{
      $meetings[$meeting_inc]['latlong'] = strval($postLocation->lat).",".strval($postLocation->long);  
    }

    $meeting_module = get_post_meta($post_id, 'rencontre_module', true);
    $meetings[$meeting_inc]['module']= $meeting_module;
    $meetings[$meeting_inc]['date1'] = "";
    if((null !== get_post_meta($post_id, 'rencontre_date_1', true)) && (get_post_meta($post_id, 'rencontre_date_1', true)!="")){
      $meetings[$meeting_inc]['date1'] .= get_post_meta($post_id, 'rencontre_date_1', true);
    }
    if(($meetings[$meeting_inc]['date1'] != "")&& (null !== get_post_meta($post_id, 'rencontre_heure_1', true)) && (get_post_meta($post_id, 'rencontre_heure_1', true)!="")){
      $meetings[$meeting_inc]['date1'] .= " à ".get_post_meta($post_id, 'rencontre_heure_1', true);
    }
    $meetings[$meeting_inc]['date2']= "";
    if((null !==get_post_meta($post_id, 'rencontre_date_2', true)) && (get_post_meta($post_id, 'rencontre_date_2', true)!="")){
      $meetings[$meeting_inc]['date2'] .= get_post_meta($post_id, 'rencontre_date_2', true);
    }
    if(($meetings[$meeting_inc]['date2'] != "")&& ( null !== get_post_meta($post_id, 'rencontre_heure_2', true)) && (get_post_meta($post_id, 'rencontre_heure_2', true)!="")){
      $meetings[$meeting_inc]['date2'] .= " à ".get_post_meta($post_id, 'rencontre_heure_2', true);
    }
    $meetings[$meeting_inc]['url']= get_site_url()."/?post_type=rencontre&p=".$post_id;
    //calcul de la distance
    $meetings[$meeting_inc]['distance'] ='';
    if (is_user_logged_in()) {
      $user_id = wp_get_current_user()->ID;
      $postCoord = rencontre_post_type::get_location($post_id, "posts", "coordinates");
      $userCoord = rencontre_post_type::get_location($user_id, "members", "coordinates");
      if($userCoord && $postCoord){
        $meetings[$meeting_inc]['distance'] = round(rencontre_post_type::get_location_distance($postCoord,$userCoord ),1);  
      }
    }
    
    
    $meeting_inc++;
    //counters
    $allNbre++;
    if($rencontre['future']){
      $futureNbre++;  
    }
    if($rencontre['past']){
      $pastNbre++;  
    }
    if(isset($modulesNbreArray[$meeting_module])){
      $modulesNbreArray[$meeting_module]++;
    }else{
      $modulesNbreOther++;
    }
    if($rencontre['nearby']){
      $nearbyNbre++;  
    }
    //end counter
  }

  $htmlForm = '<div id="classCodeMeetingMainDisplay">';
  $htmlForm.= '<div id="classCodeMeetingMap" class="contentMedia"></div>';
  $htmlForm.= '<div id="classCodeMeetingSearchDesc" class="contentDesc">';
  $htmlForm.= '<form method="post" id="meetingSearchForm" action="/classcode/rencontres/#anchorSearchform">';
  $htmlForm.= '<input type="hidden" value="true" id="search" name="search">';
  $htmlForm.= '<h3>Rencontres</h3>';
  $checked = '';
  if(isset($_POST['mymeeting']) && ($_POST['mymeeting'] == 'true')){
    $checked = 'checked';
  }
  if(is_user_logged_in()) {
    $htmlForm.= '<input type="checkbox" name="mymeeting" '.$checked.' value="true"> Mes rencontres <br/>';
  }
  if(isset($_POST['structure']) && ($_POST['structure'] != '')){
    $default_structure = $_POST['structure'];
  }else{
    $default_structure = '';
  }
  $htmlForm.= do_completion_field("structure", array_keys($data_structures),$default_structure , "", false, "Filtrer par Structure");       
  
  $htmlForm.= '<h3>Dates</h3>';
  $htmlForm.= '<input type="radio" name="when" value="all" checked> Toutes ';
  
  $htmlForm.= '<br/>';
  $checked = '';
  if(isset($what['when']) && ($what['when'] == 'past')){
    $checked = 'checked';
  }
  $htmlForm.= '<input type="radio" name="when" value="past" '.$checked.'> Passées ';
 
  $htmlForm.= '<br/>';

  $checked = '';
  if(isset($what['when']) && ($what['when'] == 'future')){
    $checked = 'checked';
  }
  $htmlForm.= '<input type="radio" name="when" value="future" '.$checked.'> A venir ';

  $htmlForm.= '<br/>';
  
  $htmlForm.= '<h3>Modules</h3>';
  $htmlForm.= '<input type="radio" name="module" value="all" checked> Tous ';

  $htmlForm.= '<br/>';

  foreach($title_module as $title) { 
    $checked = '';
    if(isset($what['module']) && ($what['module'] == $title)){
      $checked = 'checked';
    }
    $htmlForm.= '<input type="radio" name="module" value="'.$title.'" '.$checked.'>'.$title;
  
    $htmlForm.= '<br/>';
  }
  $checked = '';
  if(isset($what['module']) && ($what['module'] == 'other')){
    $checked = 'checked';
  }
  $htmlForm.= '<input type="radio" name="module" value="other" '.$checked.'> Autre ';

  $htmlForm.= '<br/>';
  if(is_user_logged_in()) {
    $htmlForm.= '<h3>Localisation</h3>';
    $htmlForm.= '<input type="radio" name="where" value="everywhere" checked> Partout ';
    $htmlForm.= '<br/>';
      
    $htmlForm.= 'Près de chez moi :';
    $checked = '';
    if(isset($what['nearby']) && ($what['nearby'] == '20')){
      $checked = 'checked';
    } 
    $htmlForm.= '<input type="radio" name="where" value="20" '.$checked.'> 20 km ';
    $checked = '';
    if(isset($what['nearby']) && ($what['nearby'] == '100')){
      $checked = 'checked';
    } 
    $htmlForm.= '<input type="radio" name="where" value="100" '.$checked.'> 100 km';
    $checked = '';
    if(isset($what['nearby']) && ($what['nearby'] == '200')){
      $checked = 'checked';
    } 
    $htmlForm.= '<input type="radio" name="where" value="200" '.$checked.'> 200 km';
    $htmlForm.= '<br/>';
  }
  $htmlForm.= '</form>';
  $htmlForm.= '</div>';
  $htmlForm.= '</div>';
  $htmlForm.= '<div id="classCodeMeetingMainFooter">';
  $htmlForm.= '<div id="classCodeMeetingMapFooter" class="contentMedia">';
  if (is_user_logged_in()) {
    $htmlForm.= '<a href="/classcode/rencontres/creer-une-rencontre">Créer une rencontre</a>';
  }
  $htmlForm.= '</div>'; 
  $htmlForm.= '<div id="classCodeMeetingMapSearch" class="contentDesc"><a onclick="searchMeeting();">Rechercher</a></div>';
 
  $htmlForm.= '</div>';

  $htmlForm.= '<script type="text/javascript">';
  $htmlForm.= 'var geocoder;';
  $htmlForm.= 'var map;';
  $htmlForm.= 'var marker;';
  
  $htmlForm.= "var meetingMarkerImage = '".get_template_directory_uri()."/_img/classcode_pictos/meeting/Img-rencontre-07.png';";
  $htmlForm.= "var visitorMarkerImage = '".get_template_directory_uri()."/_img/classcode_pictos/meeting/Img-rencontre-07-2.png';";

  $htmlForm.= 'jQuery( document ).ready(function() {';
  $htmlForm.= "var latlng = new google.maps.LatLng(".$visitorLatLong.");";
  $htmlForm.= 'loadGmap(latlng);';
  $htmlForm.= '});';


  $htmlForm.= 'function loadGmap(latlng) {';
    
  $htmlForm.= 'var options = {center: latlng,zoom: '.$mapZoom.',mapTypeId: google.maps.MapTypeId.ROADMAP};';
  $htmlForm.= 'map = new google.maps.Map(document.getElementById("classCodeMeetingMap"), options);';
  $htmlForm.= 'var oms = new OverlappingMarkerSpiderfier(map);';
  $htmlForm.= 'geocoder = new google.maps.Geocoder();';
  $htmlForm.= 'var infowindow = new google.maps.InfoWindow();';
  if(!$noVisitorLocation){
    $htmlForm.= "var visitorDisplayName ='<div class=\"meetingMapInfoTitle\">".$visitorDisplayName."</div>';";
   // $htmlForm.= "var visitorInfoWindow = new google.maps.InfoWindow({content: visitorDisplayName});";
    $htmlForm.= "marker = new google.maps.Marker({map: map,icon: visitorMarkerImage,position: latlng,title: '".$visitorDisplayName."',});";  
    $htmlForm.= "marker.desc=visitorDisplayName;";
    $htmlForm.= "oms.addMarker(marker); ";
   // $htmlForm.= "marker.addListener('click', function() {visitorInfoWindow.open(map,marker);});";

  }  
  $htmlForm.= 'var meetingArrayMarker = [];';
  $htmlForm.= 'var meetingContentString = [];';
  ;
  $meeting_inc = 0;
  foreach($meetings as $meeting){      
    if(isset($meeting['latlong']) && $meeting['latlong']!=''){         
        $htmlForm.= "meetingContentString[".$meeting_inc."] = '<div class=\"meetingMapInfoTitle\">".addslashes($meeting['module'])."</div>';";
        $htmlForm.= "meetingContentString[".$meeting_inc."]+=  '".($meeting['structure'] ? "<div>".addslashes($meeting['structure'])."</div>" : "")."';";
        $htmlForm.= "meetingContentString[".$meeting_inc."]+=  '<div>".addslashes($meeting['lieu'])."</div>';";
        $htmlForm.= "meetingContentString[".$meeting_inc."]+=  '".($meeting['date1'] ? "<div>Le ".$meeting['date1']."</div>" : "")."';";
        $htmlForm.= "meetingContentString[".$meeting_inc."]+=  '".($meeting['date2'] ? "<div>Le ".$meeting['date2']."</div>" : "")."';";
        if (is_user_logged_in()) {
          $htmlForm.= "meetingContentString[".$meeting_inc."]+=  '<a href=\"".$meeting['url']."\">Voir la rencontre</a>';";
        }
       // $htmlForm.= "meetingInfowindow[".$meeting_inc."] = new google.maps.InfoWindow({content: meetingContentString[".$meeting_inc."]});";
        $htmlForm.= "var meetingLatLong = new google.maps.LatLng(".$meeting['latlong'].");";
        
        $htmlForm.= "meetingArrayMarker[".$meeting_inc."] = new google.maps.Marker({map: map,icon: meetingMarkerImage,position: meetingLatLong,title: 'Temps de rencontre',});";
        $htmlForm.= "meetingArrayMarker[".$meeting_inc."].desc=meetingContentString[".$meeting_inc."];";
        $htmlForm.= "oms.addMarker(meetingArrayMarker[".$meeting_inc."]); ";
        
        //$htmlForm.= "meetingArrayMarker[".$meeting_inc."].addListener('click', function() { meetingInfowindow[".$meeting_inc."].open(map, meetingArrayMarker[".$meeting_inc."]);});";

    }
    $meeting_inc ++;
  }
  
  $htmlForm.= "oms.addListener('click',function(marker, event) {";
  $htmlForm.= "infowindow.setContent(marker.desc);";
  $htmlForm.= "infowindow.open(map, marker);";
  $htmlForm.="});";
        
  $htmlForm.= "oms.addListener('spiderfy', function(meetingArrayMarker) {";
  $htmlForm.= "  infowindow.close();";
  $htmlForm.= "});";

  
  $htmlForm.= "}";

  $htmlForm.= "function searchMeeting(){";    
  $htmlForm.= "window.onbeforeunload = null;";
  $htmlForm.= "var meetingSearchForm = document.getElementById('meetingSearchForm');";
  $htmlForm.= "jQuery('<input type=\"submit\">').hide().appendTo(meetingSearchForm).click().remove();";
  $htmlForm.= "return false;";
  $htmlForm.= "}";
  $htmlForm.= "</script>";
  
  return $htmlForm;
}
add_shortcode("cc_meeting_search", "cc_meeting_search_shortcode");

?>