<h1 style="margin-left:10px;">Le lexique des sciences du numérique</h1>

<section id="lexique">

<?php $lexique_url="?".preg_replace("/&alphabeticpage=./", "", $_SERVER["QUERY_STRING"]); ?>
                  
   <ul>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=a" class="lettres">A</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=b" class="lettres">B</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=c" class="lettres">C</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=d" class="lettres">D</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=e" class="lettres">E</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=f" class="lettres">F</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=g" class="lettres">G</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=h" class="lettres">H</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=i" class="lettres">I</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=j" class="lettres">J</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=k" class="lettres">K</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=l" class="lettres">L</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=m" class="lettres">M</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=n" class="lettres">N</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=o" class="lettres">O</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=p" class="lettres">P</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=q" class="lettres">Q</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=r" class="lettres">R</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=s" class="lettres">S</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=t" class="lettres">T</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=u" class="lettres">U</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=v" class="lettres">V</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=w" class="lettres">W</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=x" class="lettres">X</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=y" class="lettres">Y</a></li>
     <li><a href="<?php echo $lexique_url;?>&alphabeticpage=z" class="lettres">Z</a></li>
   </ul>
      
</section>
